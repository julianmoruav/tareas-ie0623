;##############################################################################
;                                 Tarea #6
;   Fecha: 14 de noviembre del 2019.
;   Autor: Julian Morua Vindas
;
;   Descripcion: este programa simula el control de nivel de un tanque. Realiza
;     mediciones de una entrada analogica y la convierte a digital, esta entrada
;     representa el nivel. A partir del nivel se obtiene el volumen y con el
;     volumen se enciende y apaga un relay segun el estado del tanque, para
;     simular el encendido y apagado de la bomba de llenado del tanque. Adicionalmente,
;     el volumen del tanque junto con otros mensajes son transmitidos a una terminal
;     (que debe ser configurada por aparte) en la computadora a traves de la
;     interfaz de comunicacion serial SC1.
;##############################################################################
#include registers.inc

HMAX:           EQU 25 ;metros
AREA_M2_x10:    EQU 196 ;metros cuadrados escalado por 10 (el area de la base es 19.63 m2)
PLENA_ESCALA:   EQU 30 ;metros
CUANTOS_10:     EQU 1024 ;conversiones a 10 bits --> 2^10 cuantos.
CUANTOS_8:      EQU 256 ;conversiones a 8 bits --> 2^8 cuantos.
PORCIENTO_15:   EQU 74 ;15% del volumen maximo 490
PORCIENTO_30:   EQU 147 ;30% del volumen maximo 490
PORCIENTO_90:   EQU 441 ;90% del volumen maximo 490

EOM:            EQU $00 ;end of message
CR:             EQU $0D ;carriage return
LF:             EQU $0A ;line feed
NP:             EQU $0C ;new page
;------------------------------------------------------------------------------
;     Declaracion de las estructuras de datos y vectores de interrupcion
;------------------------------------------------------------------------------
;Vectores de interrupcion:
                ORG $3E52 ;direccion del vector de interrupcion ATD0.
                DW ATD0_ISR ;direccion de la subrutina de servicio a interrupcion ATD0.
                ORG $3E54 ;direccion del vector de interrupcion SCI1.
                DW SCI_ISR ;direccion de la subrutina de servicio a interrupcion SCI1.
                ORG $3E64   ;direccion del vector de interrupcion OC5.
                DW OC5_ISR  ;direccion de la subrutina de servicio a interrupcion OC5.

;Estructuras de datos:
                ORG $1010
NIVEL_PROM:     DS 2 ;almacena el promedio de las 6 mediciones a 10 bits del ATD.
NIVEL:          DS 1 ;almacena el valor redondeado a 8 bits de NIVEL_PROM en metros.
VOLUMEN:        DS 2 ;almacena el volumen del tanque en metros cubicos (m3).
CONT_OC:        DS 1 ;permite generar la cadencia de 1 seg en el OC5 para la transmision.

BCD_H:          DS 1 ;miles y centenas en BCD
BCD_L:          DS 1 ;decenas y unidades en BCD
LOW:            DS 1 ;variable temporal
PRINT_PTR:      DS 2 ;con este puntero se maneja la impresion caracter por caracter.
FLAGS:          DS 1 ; X:X:X:X:X:MSG_SEL:MSG:DONE

;Mensajes:
MENSAJE:        FCC "MEDICION DE VOLUMEN"
                DB CR,LF
                FCC "VOLUMEN ACTUAL: "
CENTENAS:       DS 1
DECENAS:        DS 1
UNIDADES:       DS 1
                FCC " m3."
                DB CR,LF,EOM

MSG_ALARM:     FCC "Alarma: el nivel esta bajo."
                DB EOM

MSG_FULL:      FCC "Tanque lleno, bomba apagada."
                DB EOM
;*******************************************************************************
;                             Programa principal
;*******************************************************************************
;------------------------------------------------------------------------------
;                          Configuracion del hardware
;------------------------------------------------------------------------------
    ORG $2000
;Configuracion de la salida SAL: relay en puerto PORTE4.
    MOVB #$04,DDRE ;se configura PORTE4 como salida

;Configuracion del ATD0:
    MOVB #$C2,ATD0CTL2 ;habilita convertidor ATD0, borrado rapido de banderas y las interrupciones.
    LDAA #160
CONFIG_ATD:
    DBNE A,CONFIG_ATD ;3ciclosCLK * 160cuentas * (1/48 MHz) = 10 us. Tiempo requerido para que inicie el ATD.
    MOVB #$30,ATD0CTL3 ;ciclo de 6 conversiones
    MOVB #$10,ATD0CTL4 ;conversion a 10 bits, tiempo final de muestreo es 2 periodos, f_sample = 700 kHz (PRS~16)
    MOVB #$87,ATD0CTL5 ;justifica a la derecha. Sin signo. No multiplexacion. Canal 7 es el del pot. Escritura inicia ciclo.

;Configuracion del modulo de Timer como Output Compare en el Canal 5:
    BSET TSCR1 %10010000 ;se habilita modulo de timer y borrado rapido de banderas.
    BSET TSCR2 %00000110 ;prescaler es 2^6 = 64
    BSET TIOS %00100000 ;se configura el canal 5 como Output Compare.
    BSET TIE %00100000 ;se habilita interrupcion del canal 5.
    BCLR TCTL1 $00001100 ;no es necesario que haya una salida en puerto T. Solo se requiere la interrupcion.

;Configuracion del Serial Communication Interface (SCI1):
    MOVW #39,SC1BDH ;Baud Rate = 38400
    MOVB #%00010010,SC1CR1 ;M = 1, ParityEnable = 1, ParityType = 0 (par).
    MOVB #%00001000,SC1CR2 ;T = 1

    CLI
;------------------------------------------------------------------------------
;                       Inicializacion de variables
;------------------------------------------------------------------------------
    LDS #$3BFF  ;inicializa el stack
    MOVB #75,CONT_OC
    CLR FLAGS
    LDD TCNT ;se carga el valor actual de TCNT para reajustar el output compare
    ADDD #5000
    STD TC5 ;se actualiza el nuevo valor a alcanzar.
;------------------------------------------------------------------------------
MAIN_LOOP:
    JSR CALCULO
    JSR BIN_BCD_9999
    JSR BCD_ASCII_999
    JSR DETERMINE_FLAGS
    BRA MAIN_LOOP
;*******************************************************************************


;------------------------------------------------------------------------------
;   Subrutina CALCULO: la variable NIVEL_PROM contiene la medicion del tanque a
;     una resolucion de 10 bits, esta subrutina convierte este valor a una escala
;     con resolucion de 8 bits y lo guarda en la variable NIVEL. Ademas, a partir
;     del NIVEL se obtiene el volumen del tanque en metros cubicos y se guarda
;     en la variable VOLUMEN.
;------------------------------------------------------------------------------
CALCULO:
    LDD #CUANTOS_10-1
    LDX #CUANTOS_8-1
    IDIV ;X = D/X = 1023/255 ~ 4.   Los cuantos a 8 bits valen 4 veces mas en escala que los cuantos a 10 bits.
    LDD NIVEL_PROM
    IDIV ;X = D/X = NIVEL_PROM/4. Se convierte la medicion a 10 bits en su valor a 8 bits.
    TFR X,D
    LDAA #PLENA_ESCALA
    MUL ;D = A*B = PLENA_ESCALA*NIVEL
    LDX #CUANTOS_8-1
    IDIV ;X = D/X = NIVEL*[PLENA_ESCALA/(CUANTOS_8-1)] = NIVEL*[RESOLUCION] = ALTURA en metros.
    TFR X,D
    CMPB #HMAX
    BLS CALC_VOL
    LDAB #HMAX ;este programa simula el sensor del tanque con el potenciometro, por lo  que si se podria tener la medicion maxima de 30 metros. Como el tanque es de 25 metros maximo se fuerza que este sea el nivel si la medicion es mayor.
CALC_VOL:
    STAB NIVEL ;se almacena el valor en metros de la medicion del nivel, a una resolucion de 8 bits.
    LDAA #AREA_M2_x10 ;Se carga el valor del area, precalculado a mano pues es constate.
    MUL ;D = A*B = AREA_M2*10*NIVEL = VOLUMEN*10
    LDX #10
    IDIV ;X = D/X = VOLUMEN*10/10 = VOLUMEN
    STX VOLUMEN
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina BIN_BCD_9999: esta subrutina convierte un valor binario entre 0 y 9999
;     a BCD. Cada digitio en BCD requiere un nibble, por lo que se usan las variables
;     BCD_H y BCD_L para almacenar cada uno de ellos. La variable a convertir es VOLUMEN.
;------------------------------------------------------------------------------
BIN_BCD_9999:
    LDD VOLUMEN
    LDX #15 ;hay 16 bits, pero el ultimo desplazamiento se hace manualmente
    CLR BCD_H
    CLR BCD_L
LOOP_BIN_BCD:
    LSLD
    ROL BCD_L
    ROL BCD_H
    PSHD
    LDAA #$0F ;mascara
    ANDA BCD_L ;se obtienen las unidades
    CMPA #5
    BLO NO_ADD_U
    ADDA #3
NO_ADD_U:
    STAA LOW
    LDAA #$F0 ;mascara
    ANDA BCD_L ;se obtienen las decenas
    CMPA #$50
    BLO NO_ADD_D
    ADDA #$30
NO_ADD_D:
    ADDA LOW
    STAA BCD_L
    LDAA #$0F ;mascara
    ANDA BCD_H ;se obtienen las centenas
    CMPA #5
    BLO NO_ADD_C
    ADDA #3
NO_ADD_C:
    STAA LOW
    LDAA #$F0 ;mascara
    ANDA BCD_H ;se obtienen los miles
    CMPA #$50
    BLO NO_ADD_M
    ADDA #$30
NO_ADD_M:
    ADDA LOW
    STAA BCD_H
    PULD
    DEX
    BNE LOOP_BIN_BCD
    LSLD
    ROL BCD_L
    ROL BCD_H
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina BCD_ACII_999: esta subrutina toma un numero en BCD de tres digitos
;     y convierte cada uno de sus digitos en su correspondiente codificacion ASCII.
;     Se guardan en las variables CENTENAS, DECENAS y UNIDADES.
;------------------------------------------------------------------------------
BCD_ASCII_999:
    LDAA #$0F ;mascara
    LDAB #$F0 ;mascara
    ANDA BCD_L ;se obtienen las unidades
    ADDA #$30 ;conversion a ASCII
    STAA UNIDADES
    ANDB BCD_L ;se obtienen las descenas
    LSRB
    LSRB
    LSRB
    LSRB ;se deben desplazar a la parte baja del byte
    ADDB #$30 ;conversion a ASCII
    STAB DECENAS
    LDAA #$0F
    ANDA BCD_H ;se obtienen las centenas
    ADDA #$30 ;conversion a ASCII
    STAA CENTENAS
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina DETERMINE_FLAGS: a partir del VOLUMEN calculado, esta subrutina
;     activa el relay cuando se esta por debajo del 15% y lo desactiva cuando
;     se esta por encima de 90%. Ademas modifica las banderas MSG y MSG_SEL para
;     que cuando se refresque la terminal se impriman los mensajes de alarma y
;     tanque lleno cuando corresponda.
;------------------------------------------------------------------------------
DETERMINE_FLAGS:
    LDD VOLUMEN
    CPD #PORCIENTO_15 ;se compara el volumen con el 15%
    BLS RELAY_ON ;si es menor se debe encender la alarma
    CPD #PORCIENTO_90 ;se compara el volumen con el 90%
    BHS RELAY_OFF ;si es mayor se debe apagar la bomba
    CPD #PORCIENTO_30 ;se compara el volumen con el 30%
    BHS NO_PRINT ;si es mayor (ya se sabe que es menor que 90%) no hay nada que hacer.
    BRCLR FLAGS %00000100 PRINT ;Si MSG_SEL=0
    BRA NO_PRINT


RELAY_ON:
    BSET PORTE %00000100 ;enciende la bomba
    BCLR FLAGS %00000100 ;MSG_SEL <--- 0 para indicar que el mensaje a imprimir es el de alarma
    BRA PRINT

RELAY_OFF:
    BCLR PORTE %00000100 ;se apaga la bomba
    BSET FLAGS %00000100 ;MSG_SEL <--- 1 para indicar que el mensaje a imprimir es el de tanque lleno
PRINT:
    BSET FLAGS %00000010 ;MSG <-- 1 para indicar que hay que imprimir otro mensaje
    BRA FIN_DETERMINE

NO_PRINT:
    BCLR FLAGS %00000010 ;MSG <-- 0 para indicar que NO hay que imprimir otro mensaje
FIN_DETERMINE:
    RTS
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina ATD0_ISR: luego de que el ATD realiza las 6 mediciones al canal 7
;     esta subruina calcula el promedio de esas mediciones y lo guarda en la
;     variable NIVEL_PROM. Ademas inicia el siguiente ciclo de conversion.
;------------------------------------------------------------------------------
ATD0_ISR:
    LDD ADR00H
    ADDD ADR01H
    ADDD ADR02H
    ADDD ADR03H
    ADDD ADR04H
    ADDD ADR05H ;En D se tiene la sumatoria de las mediciones al canal 7 del ATD.
    LDX #6 ;carga el divisor de 6 para calcular el promedio
    IDIV ;X <-- D/X = sumatoria/6 = promdio
    STX NIVEL_PROM ;se almacena el promedio en la variable designada.
    MOVB #$87,ATD0CTL5 ;al escribir a ATD0CTL5 se inicia un nuevo ciclo de conversion
    RTI
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina OC5_ISR: el canal 5 del output compare interrumpe a una frecuencia
;     de 75 Hz y por medio de la variable CONT_OC se logra una cadencia de 1 Hz
;     en la cual se refresca la informacion de la terminal. Para hacer esto,
;     cuando el contador CONT_OC (precargado con 75) alcanza un valor de 0, se
;     habilita la interrupcion del SCI1 y se inicia una transmision.
;------------------------------------------------------------------------------
OC5_ISR:
    TST CONT_OC
    BEQ REFRESCAR
    DEC CONT_OC
    BRA FIN_OC5
REFRESCAR:
    MOVW #MENSAJE,PRINT_PTR ;se carga el inicio del mensaje al puntero de impresion.
    BSET SC1CR2 %01000000 ;Interrupciones de transmisor: TCIE = 1
    LDAA SC1SR1 ;primer paso para iniciar transmision
    MOVB #NP,SC1DRL ;se escribe el dato a transmitir para iniciar la transmision.
    MOVB #75,CONT_OC ;se recarga el contador para la cadencia de 1 Hz.
FIN_OC5:
    LDD TCNT
    ADDD #5000 ;5000 cuentas en TCNT equivale a interrupciones de OC5 a 75 Hz, con PRS=64
    STD TC5 ;escribir a TC5 borra la bandera de interrupcion porque esta activado el borrado rapido
    RTI
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina SCI_ISR: esta subrutina realiza la transmision, 1 byte por cada
;     interrupcion, a traves de la interfaz SC1. Cuando termina de transimitir
;     todo el mensaje se deshabilita la interrupcion.
;------------------------------------------------------------------------------
SCI_ISR:
    LDX PRINT_PTR ;se carga el puntero de impresion de bytes
    LDAA 1,X+ ;se obtiene el caracter que se debe imprimir.
    CMPA #EOM ;se comprueba que el caracter no sea el final de la hilera de caracteres
    BEQ MENSAJES ;si es el final, se deben revisar los casos especiales de impresion
    LDAB SC1SR1 ;primer paso para iniciar transmision
    STAA SC1DRL ;se escribe el dato a transmitir para iniciar la transmision.
    STX PRINT_PTR ;se almacena el puntero de impresion.
    BRA FIN_SCI ;se termina la subrutina y se espera a que vuelva a interrumpir luego de transmitir el byte.
MENSAJES:
    BRSET FLAGS %00000001 SCI_OFF ;Si DONE = 1 no hay nada mas por imprimir y se debe desahibilitar la interrupcion SCI
    BRCLR FLAGS %00000010 SCI_OFF ;Si MSG = 0 es porque no hay que imprimir ningun mensaje adicional.
    BRCLR FLAGS %00000100 PRINT_ALARM ;Si MSG_SEL = 0 el mensaje que se debe imprimir es el de alarma.

    MOVW #MSG_FULL,PRINT_PTR ;se carga el mensaje de tanque lleno
    BSET FLAGS %00000001 ;DONE <-- 1 para que en el proximo caracter EOM se termine el refrescamiento de la pantalla.
    BRA FIN_SCI

PRINT_ALARM:
    MOVW #MSG_ALARM,PRINT_PTR ;se carga el mensaje de alarma
    BSET FLAGS %00000001 ;DONE <-- 1 para que en el proximo caracter EOM se termine el refrescamiento de la pantalla.
    BRA FIN_SCI

SCI_OFF:
    BCLR SC1CR2 %01000000 ;Se deshabilita la interrupcion SPI. TCIE = 0.
    BCLR FLAGS %00000001 ;DONE <-- 0 para que la bandera quede lista para el proximo refrescamiento

FIN_SCI:
    RTI
;------------------------------------------------------------------------------
