;##############################################################################
;                                 Tarea #4
;   Fecha: 25 de octubre del 2019.
;   Autor: Julian Morua Vindas
;
;   Descripcion: En esta tarea se implementa un programa que permite leer el
;       teclado matricial de la Dragon 12 en el puerto A, y tomar estas lecturas
;       para formar un arreglo que representa la secuencia de teclas presionadas
;       como un numero en BCD, almacenado en un arreglo donde cada posicion es
;       un digito del numero. Ademas, se implementa una funcionalidad de borrado
;       que permite corregir el valor ingresado cuando se presiona la tecla '*'.
;       Cuando ya no se desean ingresar mas valores se usa la tecla '#' para
;       finalizar la lectura.
;                         -------------
;                         | 1 | 2 | 3 |
;                         | 4 | 5 | 6 |
;                         | 7 | 8 | 9 |
;                         | B | 0 | E |
;                         -------------
;       El programa implementa supresion de rebotes y contempla la situacion de
;       tecla retenida. Una interrupcion keywakeup en el puerto PH0 permite
;       reiniciar el programa.
;##############################################################################
#include registers.inc
MAX: EQU 4 ;cambie este valor para cambiar la cantidad maxima de valores que se aceptan. Debe ser igual o menor a 6.

;------------------------------------------------------------------------------
;     Declaracion de las estructuras de datos y vectores de interrupcion
;------------------------------------------------------------------------------
;Estructuras de datos:
                ORG $1000
MAX_TCL:        DB MAX  ;cantidad maximas de teclas que se leen
Tecla:          DS 1  ;en esta variable se almacena el valor leido del teclado en la subrutina MUX_TECLADO.
Tecla_IN:       DS 1  ;en esta variable se almacena temporalmente el valor de Tecla antes de la supresion de rebotes. Si despues de la supresion se da que Tecla y Tecla_IN son iguales es porque efectivamente se presiono una tecla que debe ser guardada.
Cont_Reb:       DS 1  ;es el contador de ticks del RTI, usado para suprimir rebotes.
Cont_TCL:       DS 1  ;es el indice utilizado para escribir en el arreglo que guarda las teclas presionadas.
Patron:         DS 1  ;es el indice que lleva las iteraciones en subrutina MUX_TECLADO.
Banderas:       DS 1  ;Tiene el formato: X:X:X:X:X:ARRAY_OK:TCL_LEIDA:TCL_LISTA.
                      ;ARRAY_OK indica que se presiono la tecla Enter y que en el arreglo ya se tienen todos los valores leidos.
                      ;TCL_LEIDA indica que ya se habia tenido una lectura del teclado y que se estaba esperando a que se diera la supresion de rebotes.
                      ;TCL_LISTA indica que luego de la supresion de rebotes se confirmo que si se presiono una tecla.
Num_Array:      DS 6  ;en este arreglo se almacenan todas las teclas presionadas.
Teclas:         DB $01,$02,$03,$04,$05,$06,$07,$08,$09,$0B,$00,$0E

;Vectores de interrupcion:
                ORG $3E70   ;direccion del vector de interrupcion RTI.
                DW RTI_ISR  ;direccion de la subrutina de servicio a interrupcion RTI.
                ORG $3E4C   ;direccion del vector de interrupcion por key wakeup del puerto H.
                DW PH0_ISR  ;direccion de la subrutina de servicio a interrupcion del puerto H.
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;                          Configuracion del hardware
;------------------------------------------------------------------------------
                ORG $1500
;Configuracion RTI:
                BSET CRGINT %10000000 ;se habilita RTI
                MOVB #$31,RTICTL      ;periodo de 1.024 ms

;Configuracion keywakeup en puerto H:
                BSET PIEH %00000001   ;se habilita keywakeup en PH0

;Configuracion del teclado en puerto A:
                MOVB #$F0,DDRA        ;parte alta de A como salida y parte baja como entrada
                BSET PUCR %00000001   ;resistencias de pull-up en puerto A. Son necesarias para que haya un 1 en el PAD cuando no se presiona ningun boton del teclado.

                LDS #$3BFF  ;inicializa el stack
                CLI        ;habilita interrupciones mascarables.
;------------------------------------------------------------------------------


;*******************************************************************************
;                             Programa principal
;*******************************************************************************
;inicializacion de variables:
                MOVB #$FF,Tecla
                MOVB #$FF,Tecla_IN
                MOVB #$FF,Num_Array
                CLR Cont_Reb
                CLR Banderas
                CLR Cont_TCL
                CLR Patron

ESPERE:         BRSET Banderas,%00000100,ESPERE ;si ARRAY_OK=1 no se espera lectura.
                JSR TAREA_TECLADO
                BRA ESPERE
;*******************************************************************************


;------------------------------------------------------------------------------
; Subrutina TAREA_TECLADO: En esta subrutina se da la lectura del teclado. Aqui
;     se lee el teclado en el puerto A, se suprimen los rebotes, y se maneja la
;     situacion de tecla retenida.
;------------------------------------------------------------------------------
TAREA_TECLADO:  TST Cont_Reb
                BNE FIN_TAREA ;si el contador no ha llegado a cero no se han suprimido los rebotes.
                JSR MUX_TECLADO ;se lee el teclado en el puerto A.
                LDAA Tecla
                CMPA #$FF ;el valor $FF indica que no hay tecla presionada.
                BEQ GUARDAR ;si se toma el salto es porque es posible que haya una tecla lista para ser guardada en el arreglo. Al hacerlo solo cuando Tecla = $FF se evita la situacion de tecla retenida.
                BRSET Banderas,%00000010,CONFIRMACION ;cuando la bandera TCL_LEIDA=1 se debe confirmar que la tecla antes y despues de los rebotes es la misma
                MOVB Tecla,Tecla_IN ;se almacena el valor de la tecla antes de suprimir rebotes
                BSET Banderas %00000010 ;TCL_LEIDA<-1 para indicar que se esta esperando a que termine la supresion de rebotes.
                MOVB #10,Cont_Reb ;se inicia la cuenta de ticks para la supresion de rebotes.
FIN_TAREA:      RTS

GUARDAR:        BRCLR Banderas,%00000001,FIN_TAREA ;si TCL_LISTA=0 no hay nada que hacer
                BCLR Banderas %00000011 ;se limpian lsa banderas TCL_LISTA y TCL_LEIDA
                JSR FORMAR_ARRAY
                BRA FIN_TAREA

CONFIRMACION:   LDAA Tecla
                CMPA Tecla_IN
                BEQ LECTURA ;si la tecla antes y despues de la supresion de rebotes es igual entonces se dio una lectura correcta
                MOVB #$FF,Tecla ;se pone el valor nulo para la tecla
                MOVB #$FF,Tecla_IN ;se pone el valor nulo para la tecla
                BCLR Banderas %00000011 ;se limpian lsa banderas TCL_LISTA y TCL_LEIDA
                BRA FIN_TAREA
LECTURA:        BSET Banderas %00000001 ;TCL_LISTA=1 indica una lectura correcta del teclado.
                MOVB #10,Cont_Reb ;se inicia la cuenta de ticks para la supresion de rebotes.
                BRA FIN_TAREA
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
; Subrutina MUX_TECLADO: esta subrutina es la que se encarga de leer el teclado
;     en el puerto A. Como se habilitaron las resistencias de pull up, la parte
;     baja del puerto A (la entrada) siempre tendra valor %1111. Al colocar un
;     unico cero en alguno de los bits de la parte alta, si se presiona un boton
;     entonces se tendra un cero tambien en algun bit de la parte baja. Ambos
;     ceros en parte alta y baja definen la fila y columna de la tecla presionada.
;     Con esta informacion se calcula la posicion en el arreglo de teclas en el
;     que se encuentra el valor de la tecla presionada. El valor de la tecla se
;     devuelve en la variable Tecla. Si no se presiona nada entonces Tecla = $FF.
;------------------------------------------------------------------------------
MUX_TECLADO:    MOVB #$EF,PATRON  ;se inicializa el indice/patron
                LDAA #$F0 ;condicion de parada del loop. Luego de desplazar a PATRON cuatro veces este es el valor.
                CLRB   ;se inicaliza el offset por acumulador B
LOOP_PATRONES:  MOVB PATRON,PORTA
                BRCLR PORTA,%00000100,COLUMN2 ;si PA2=0 es porque se presiono una tecla en la tercer columna.
                BRCLR PORTA,%00000010,COLUMN1 ;si PA1=0 es porque se presiono una tecla en la segunda columna.
                BRCLR PORTA,%00000001,COLUMN0 ;si PA0=0 es porque se presiono una tecla en la primer columna.
                LSL PATRON;se desplaza hacia la izquierda el patron para avanzar de fila en el teclado
                ADDB #3 ;al avanzar de fila es neceasrio avanzar el offset los 3 espacios correspondientes a las 3 columnas
                CMPA PATRON ;se compara PATRON con $F0, la condicion de parada
                BNE LOOP_PATRONES ;si PATRON=$F0 es porque se recorrio todo el teclado y no se presiono ninguna tecla.
                MOVB #$FF,Tecla ;se carga el valor nulo para indicar que no se presiono una tecla
FIN_MUX:        RTS

COLUMN2:        INCB ;cuando la tecla esta en la tercer columna hay que incrementar el offset en 2.
COLUMN1:        INCB ;cuando la tecla esta en la segunda columna hay que incrementar el offset en 1.
COLUMN0:        LDX #Teclas
                MOVB B,X,Tecla ;se guarda en Tecla el valor encontrado por offset de acumulador.
                BRA FIN_MUX
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Subrutina FORMAR_ARRAY: recibe en la variable Tecla_IN el valor de la tecla
;     presionada que se quiere almacenar en array. Este arreglo representa un
;     numero en formato BCD. Se cuida que el arreglo no supere el tamano maximo.
;------------------------------------------------------------------------------
FORMAR_ARRAY:   LDX #Num_Array ;direccion base del arreglo a formar
                LDAA Tecla_IN ;la tecla presionada
                LDAB Cont_TCL ;la cantidad de valores ingresados hasta el momento
                CMPA #$0B
                BEQ BORRAR  ;se revisa si se desea borrar un valor
                CMPA #$0E
                BEQ CERRAR_ARRAY ;se revisa si se desea terminar de ingresar valores
                CMPB MAX_TCL  ;si no se dio niguno de los saltos anteriores es porque se trata de una tecla numerica
                BEQ FIN_FORMAR  ;se comprueba que haya espacio para valores nuevos, si no lo hay entonces termina la subrutina
                MOVB Tecla_IN,B,X ;se guarda el nuevo valor ingresado.
                INC Cont_TCL
FIN_FORMAR:     RTS

BORRAR:         CMPB #$00 ;para borrar primero es necesario que hayan datos que se puedan borrar
                BEQ FIN_FORMAR ;si no hay valores en el arreglo entonces termina la subrutina
                DECB
                MOVB #$FF,B,X ;se remplaza el ultimo valor ingresado por le valor nulo $FF.
                DEC Cont_TCL  ;se descuenta la cantidad de valores en el arreglo.
                BRA FIN_FORMAR

CERRAR_ARRAY:   CMPB #$00 ;para cerrar el arreglo es necesario que se haya ingresado correctamente al menos un valor.
                BEQ FIN_FORMAR ;si el arreglo esta vacio entonces termina la subrutina sin indicar que se cerro el arreglo
                CMPB MAX_TCL
                BEQ OK
                MOVB #$FF,B,X ;cuando el arreglo tiene menos valores que la cantidad maxima es necesario indicar el final.
OK:             BSET Banderas %00000100 ;ARRAY_OK=1 para indicar que se cerro el arreglo y no se deben leer mas teclas.
                CLR Cont_TCL ;para prepararlo para una futura lectura del teclado.
                BRA FIN_FORMAR
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Subrutina de servicio a interrupcion RTI: sencillamente descuenta un contador
;     siempre y cuando el contador no sea cero. Los ticks del RTI duran 1.024 ms,
;     por lo que esta interrupcion permite contar Cont_Reb milisegundos. El uso
;     que se le da en este programa es suprimir los rebotes de boton con una
;     cuenta de 10 ticks (~10 ms).
;------------------------------------------------------------------------------
RTI_ISR:        BSET CRGFLG %10000000  ;reinicia la bandera de interrupcion
                TST Cont_Reb
                BEQ FIN_RTI_ISR       ;si el contador ya esta en cero no hay nada que hacer.
                DEC Cont_Reb
FIN_RTI_ISR:    RTI
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
; Subrutina de servicio a interrupcion key wakeup del puerto PH0: esta subrutina
;     se encarga de limpiar el arrglo Num_Array y la bandera ARRAY_OK.
;------------------------------------------------------------------------------
PH0_ISR:        BSET PIFH %00000001 ;se reinicia la bandera de interrupcion
                LDX #Num_Array ;se carga en el indice la direccion base
                CLRA  ;se limpia el acumulador A para ser utilizado como offset.
                BCLR Banderas %00000100 ;ARRAY_OK=0 para indicar que se borro el arreglo y se esta leyendo el teclado
LOOP_PH0:       MOVB #$FF,A,X ;Se coloca el valor nulo de tecla
                INCA
                CMPA MAX_TCL
                BNE LOOP_PH0  ;para cada entrada del arreglo Num_Array.
                RTI
;------------------------------------------------------------------------------
