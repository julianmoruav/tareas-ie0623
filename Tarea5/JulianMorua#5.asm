;##############################################################################
;                                 Tarea #5
;   Fecha: 5 de noviembre del 2019.
;   Autor: Julian Morua Vindas
;
;   Descripcion: este programa simula el conteo y empaquetado de tornillos
;     en una linea de produccion. En el modo CONFIG se pueden definir
;     cuantos tornillos incluye cada paquete (entre 12 y 96). En el modo RUN se
;     simula el conteo de tornillos a una tasa de 4 tornillos por segundo y cuando
;     la cuenta alcanza el valor programado se aumenta un acumulador de paquetes
;     y se activa un relay dando asi indicacion de que el paquete esta completo.
;##############################################################################
#include registers.inc
MAX: EQU 2 ;cambie este valor para cambiar la cantidad maxima de valores que se aceptan. Debe ser igual o menor a 6.

;------------------------------------------------------------------------------
;     Declaracion de las estructuras de datos y vectores de interrupcion
;------------------------------------------------------------------------------
;Vectores de interrupcion:
               ORG $3E70   ;direccion del vector de interrupcion RTI.
               DW RTI_ISR  ;direccion de la subrutina de servicio a interrupcion RTI.
               ORG $3E4C   ;direccion del vector de interrupcion por key wakeup del puerto H.
               DW PTH_ISR  ;direccion de la subrutina de servicio a interrupcion del puerto H.
               ORG $3E66   ;direccion del vector de interrupcion OC4.
               DW OC4_ISR  ;direccion de la subrutina de servicio a interrupcion OC4.


;Estructuras de datos:
                ORG $1000
MAX_TCL:        DB MAX  ;cantidad maximas de teclas que se leen
Tecla:          DS 1  ;en esta variable se almacena el valor leido del teclado en la subrutina MUX_TECLADO.
Tecla_IN:       DS 1  ;en esta variable se almacena temporalmente el valor de Tecla antes de la supresion de rebotes. Si despues de la supresion se da que Tecla y Tecla_IN son iguales es porque efectivamente se presiono una tecla que debe ser guardada.
Cont_Reb:       DS 1  ;es el contador de ticks del RTI, usado para suprimir rebotes.
Cont_TCL:       DS 1  ;es el indice utilizado para escribir en el arreglo que guarda las teclas presionadas.
Patron:         DS 1  ;es el indice que lleva las iteraciones en subrutina MUX_TECLADO.
Banderas:       DS 1  ;Tiene el formato: X:X:CAMBIO_MODO:MODSEL:X:ARRAY_OK:TCL_LEIDA:TCL_LISTA.
                      ;ARRAY_OK indica que se presiono la tecla Enter y que en el arreglo ya se tienen todos los valores leidos.
                      ;TCL_LEIDA indica que ya se habia tenido una lectura del teclado y que se estaba esperando a que se diera la supresion de rebotes.
                      ;TCL_LISTA indica que luego de la supresion de rebotes se confirmo que si se presiono una tecla.
                      ;MODSEL es el selector del modo, asociado al dipswitch PH7. Modo CONFIG en ON (1), modo RUN en OFF (0).
                      ;CAMBIO_MODO cuando esta en 1 indica que se dio un cambio de modo y permite refrescar LCD.
CUENTA:         DS 1  ;cantidad de tornillos contados para cada empaque.
ACUMUL:         DS 1  ;cantidad de empaques completados. Entre 0 y 99 y puede rebasar.
CPROG:          DS 1  ;cantidad maxima de tornillos por cada empaque. Entre 12 y 96. Definido por usuario en MODO CONFIG.
VMAX:           DB 245 ;valor maximo de la variable TIMER_CUENTA. Usado para volver a iniciar la cuenta regresiva.
TIMER_CUENTA:   DS 1  ;da la cedencia de incremento de CUENTA. Simula el paso de tornillos. Decrementada por RTI_ISR.
LEDS:           DS 1  ;guarda el estado de los LEDS. LED PB1 corresponde a modo CONFIG, LED PB0 a modo RUN.
BRILLO:         DS 1  ;Variable controlada por PTH3/PTH2 para incrementar/decrementar el brillo de la pantalla LCD.
CONT_DIG:       DS 1  ;cuenta cual digito de 7 segmentos se debe habilitar. Cambia cada vez que CONT_TICKS alcanza 100.
CONT_TICKS:     DS 1  ;contador de ticks de Output Compare para multiplexar.

DT:             DS 1  ;ciclo de trabajo. DT = N-K.
BIN1:           DS 1  ;variable de entrada a subrutina BIN_BCD. Utilizada para CPROG y CUENTA.
BIN2:           DS 1  ;variable de entrada a subrutina BIN_BCD. Utilizada para ACUMUL.
LOW:            DS 1  ;variable requerida para el algoritmo de la subrutina BIN_BCD
BCD1:           DS 1  ;variable de salida de subrutina BIN_BCD. Tambien es entrada para BCD_7SEG. Utilizada para CPROG y CUENTA.
BCD2:           DS 1  ;variable de salida de subrutina BIN_BCD. Tambien es entrada para BCD_7SEG. Utilizada para ACUMUL.
DISP1:          DS 1  ;corresponde al valor que se escribe en el display de 7 segmentos.
DISP2:          DS 1  ;BCD2 utiliza DISP1 y DISP2 para desplegarse
DISP3:          DS 1  ;corresponde al valor que se escribe en el display de 7 segmentos.
DISP4:          DS 1  ;BCD1 utiliza DISP3 y DISP4 para desplegarse

CONT_7SEG:      DS 2  ;contador de ticks de OC4 para lograr refrescamiento de LEDS y Displays a 10Hz.
Cont_Delay:     DS 1  ;esta variable se carga con alguna de las siguientes tres constantes para generar retrasos temporales.
D2ms:           DB 100  ;100 ticks a 50kHz son 2 milisegundos
D260us:         DB 13  ;13 ticks a 50kHz son 260 microsegundos
D40us:          DB 2  ;2 ticks a 50kHz son 40 microsegundos
CLEAR_LCD:      DB $01  ;comando para limpiar el LCD
ADD_L1:         DB $80  ;direccion inicio de linea 1
ADD_L2:         DB $C0  ;direccion inicio de linea 2

                ORG $1030
Num_Array:      DS 6  ;en este arreglo se almacenan todas las teclas presionadas por el usuario.

                ORG $1040
Teclas:         DB $01,$02,$03,$04,$05,$06,$07,$08,$09,$0B,$00,$0E ;valores de las teclas

                ORG $1050
SEGMENT:        DB $3F,$06,$5B,$4F,$66,$6D,$7D,$07,$7F,$6F ;patrones para el display de 7 segmentos de los digitos de 0 a 9.

;LCD:
FUNCTION_SET: 	EQU $28
ENTRY_MODE_SET: EQU $06
DISPLAY_ON:     EQU $0C
CLEAR_DISPLAY: 	EQU $01
RETURN_HOME:   	EQU $02
DDRAM_ADDR1:   	EQU $80
DDRAM_ADDR2:   	EQU $C0
EOM:            EQU $00

                ORG $1060
iniDsp:         DB 4,FUNCTION_SET,FUNCTION_SET,ENTRY_MODE_SET,DISPLAY_ON

                ORG $1070 ;mensajes
CONFIG_MSJ1:    FCC "MODO CONFIG"
                DB EOM
CONFIG_MSJ2:    FCC "INGRESE CPROG:"
                DB EOM
RUN_MSJ1:       FCC "MODO RUN"
                DB EOM
RUN_MSJ2:       FCC "ACUMUL - CUENTA"
                DB EOM
;------------------------------------------------------------------------------



;*******************************************************************************
;                             Programa principal
;*******************************************************************************
;------------------------------------------------------------------------------
;                          Configuracion del hardware
;------------------------------------------------------------------------------
    ORG $2000
;Configuracion RTI:
    BSET CRGINT %10000000 ;se habilita RTI
    MOVB #$31,RTICTL      ;periodo de 1.024 ms

;Configuracion keywakeup en puerto H:
    BSET PIEH %00001100   ;se habilita keywakeup en PH2 y PH3. Note que PH0 y PH1 se habilitan en modo RUN. PH7 es por polling.
    BCLR PPSH %00001111   ;las interrupciones deben ocurrir en el flanco decreciente.

;Configuracion PH7 como entrada de proposito general por polling: (Dipswitch)
    BCLR DDRH %10000000

;Configuracion del teclado en puerto A:
    MOVB #$F0,DDRA        ;parte alta de A como salida y parte baja como entrada
    BSET PUCR %00000001   ;resistencias de pull-up en puerto A. Son necesarias para que haya un 1 en el PAD cuando no se presiona ningun boton del teclado.

;Configuracion del modulo de Timer como Output Compare en el Canal 4:
    BSET TSCR1 %10000000 ;se habilita modulo de timer.
    BSET TSCR2 %00000011 ;prescaler es 2^3 = 8
    BSET TIOS %00010000 ;se configura el canal 4 como Output Compare.
    BSET TIE %00010000 ;se habilita interrupcion del canal 4.
    BCLR TCTL1 $00000011 ;no es necesario que haya una salida en puerto T. Solo se requiere la interrupcion.

;Configuracion de los displays de 7 segmentos y los LEDS.
    MOVB #$FF,DDRB ;puerto core B se configura como salida de proposito general. (LEDS y SEGMENTOS)
    MOVB #$0F,DDRP ;parte baja de puerto P se configura como salida de proposito general. (~Habilitador Segmentos)
    BSET DDRJ %00000010 ;se configura bit 1 del puerto J como salida de proposito general . (~Habilitador LEDS)

;Configuracion de la salida SAL: relay en puerto PORTE4.
    MOVB #$04,DDRE ;se configura PORTE4 como salida

;Configuración de pantalla LCD
		MOVB #$FF,DDRK ;todos los pines del puerto K se configura como salida para controlar la LCD.

    CLI        ;habilita interrupciones mascarables.
;------------------------------------------------------------------------------
;                       Inicializacion de variables
;------------------------------------------------------------------------------
    LDS #$3BFF  ;inicializa el stack
;Teclado matricial:
    MOVB #$FF,Tecla
    MOVB #$FF,Tecla_IN
    MOVB #$FF,Num_Array
    CLR Cont_Reb
    CLR Cont_TCL
    CLR Patron

;Displays de 7 segmentos y LEDS:
    MOVB #$00,CONT_7SEG
    MOVB #$00,CONT_TICKS
    MOVB #$00,CONT_DIG
    MOVB #50,BRILLO
    MOVB #$02,LEDS
    MOVB #$00,BCD1
    MOVB #$00,BCD2

;Programa:
    CLR CPROG
    CLR CUENTA
    MOVB VMAX,TIMER_CUENTA
    CLR ACUMUL
    CLR Banderas
    BSET Banderas,%00010000 ;MODSEL=1 (MODO_CONFIG)

    LDD TCNT ;se carga el valor actual de TCNT para reajustar el output compare
    ADDD #60 ;60 cuentas equivalen 50kHz con prescalador=8
    STD TC4 ;se actualiza el nuevo valor a alcanzar.
    JSR INIT_LCD
;------------------------------------------------------------------------------
MAIN:
    TST CPROG
    BEQ INIT_CONFIG ;CPROG=0? Ir a INIT_CONFIG
    BRSET Banderas %00010000 CONFIG ;MODSEL=1? Ir a CONFIG

RUN:
    BRCLR Banderas %00100000 EX_RUN ;CAMBIO_MODO=0? Ir a EX_RUN
INIT_RUN:
    BSET PIEH %00000011 ;habilita keywakeup para PH0 y PH1
    MOVB #$01,LEDS ;PB0=ON en modo config.
    LDX #RUN_MSJ1
    LDY #RUN_MSJ2
    BCLR Banderas %00100000 ;CAMBIO_MODO=0
    JSR Cargar_LCD
EX_RUN:
    JSR MODO_RUN
    BRA COMUN

CONFIG:
    BRCLR Banderas %00100000 EX_CONFIG ;CAMBIO_MODO=0? Ir a EX_CONFIG
INIT_CONFIG:
    BSET Banderas %00010000 ;MODSEL=1. Necesario porque si CPROG=0 y PH7=0, al momento en que CPROG tenga un valor valido se pasara directo a MODO_RUN sin pasar por INIT_RUN.
    BCLR PIEH %00000011 ;deshabilita keywakeup para PH0 y PH1
    CLR CUENTA ;borra la cuenta que se llevaba puesto que se cambia el tamano del empaque
    CLR ACUMUL ;borra la cuenta de empaques completados
    MOVB #$00,PORTE ;apaga la salida del relay.
    MOVB #$02,LEDS ;PB1=ON en modo config.
    LDX #CONFIG_MSJ1
    LDY #CONFIG_MSJ2
    BCLR Banderas %00100000 ;CAMBIO_MODO=0
    JSR Cargar_LCD
EX_CONFIG:
    JSR MODO_CONFIG

COMUN:
    JSR BIN_BCD
    JSR DETERMINE_MODE
    BRA MAIN
;*******************************************************************************

;------------------------------------------------------------------------------
;   Subrutina DETERMINE_MODE: esta subrutina calcula el estado de las banderas
;     MODSEL y CAMBIO_MODO al final de cada iteracion del programa principal.
;------------------------------------------------------------------------------
DETERMINE_MODE:
    LDAA PTIH
    LSLA ;PTH7 esta en C
    BCS MODSEL1 ;si C=1 se debe poner modo CONFIG, caso contrario modo RUN.
    BRCLR Banderas %00010000 FIN_DETERMINE  ;si MODSEL=0 ya se esta en modo RUN y no hay que hacer nada
    BSET Banderas %00100000 ;CAMBIO_MODO=1
    BCLR Banderas %00010000 ;se cambia a MODO_RUN (MODSEL=0)
    LDAA CLEAR_LCD ;Cargar comando de limpiar pantalla
    JSR Send_Command ;enviar comando de limpiar pantalla
    MOVB D2ms,Cont_Delay ;luego de enviar comando limpiar pantalla se debe esperar 2ms
    JSR Delay
    BRA FIN_DETERMINE

MODSEL1:
    BRSET Banderas %00010000 FIN_DETERMINE  ;si MODSEL=1 ya se esta en modo CONFIG y no hay que hacer nada
    BSET Banderas %00100000 ;CAMBIO_MODO=1
    BSET Banderas %00010000 ;se cambia a MODO_CONFIG (MODSEL=1)
    LDAA CLEAR_LCD ;Cargar comando de limpiar pantalla
    JSR Send_Command ;enviar comando de limpiar pantalla
    MOVB D2ms,Cont_Delay ;luego de enviar comando limpiar pantalla se debe esperar 2ms
    JSR Delay

FIN_DETERMINE:
    RTS
;------------------------------------------------------------------------------




;------------------------------------------------------------------------------
;   Subrutina BCD_BIN: el arreglo Num_Array corresponde a un numero en BCD donde
;     cada entrada es un digito. Esta subrutina toma este arreglo y calcula en
;     binario el valor numerico del arreglo. El resultado se almacena en CPROG.
;------------------------------------------------------------------------------
BCD_BIN:
    LDX #Num_Array ;direccion base del numero en BCD a convertir
    CLRA ;iterador
    CLRB ;acumulador para el resultado
CIFRA:
    ADDB A,X ;se suma el digito al acumulado
    INCA ;se incrementa el indice
    PSHB ;se protege el acumulado del resultado
    LDAB #$FF ;valor que marca el final del arreglo Num_Array
    CMPB A,X  ;se compara el valor final con el proximo valor de Num_Array.
    BEQ FIN_BCD_BIN ;si son iguales es porque no hay mas numeros en el arreglo y se puede terminar la conversion
    CMPA MAX_TCL ;se compara la cantidad de valores procesados con la canitdad maxima
    BEQ FIN_BCD_BIN ;si son iguales es porque no hay mas numeros en el arreglo y se puede terminar la conversion
    PULB ;se recupera el acumulado del resultado
    PSHA ;se protege el indice
    LDAA #10 ;se prepara el multiplicador.
    MUL ;se multiplica por 10 el acumulador. En BCD cada digito esta en base 10.
    PULA ;se recupera el indice
    BRA CIFRA ;se repite lo anterior
FIN_BCD_BIN:
    PULB ;se recupera el acumulado del resultado
    STAB CPROG ;se guarda el resultado en la variable designada
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina BIN_BCD_BASE: esta subrutina realiza la conversion de un numero
;     binario entre 0 y 99 (inclusivos) a su representacion en BCD. El numero
;     a convertir se recibe como parametro por el registro A. El resultado en
;     BCD se devuelve por el registro A, donde el nibble mas significativo son
;     las decenas y el menos significativo las unidades.
;------------------------------------------------------------------------------
BIN_BCD_BASE:
    CLRB ;acumulador para el resultado.
    LDX #7 ;contador de desplazamientos.
OTRO_BIT:
    LSLA ;se extrae un bit del numero binario y queda en C.
    ROLB ;se inserta el bit en el acumulador de resultado
    PSHA ;se protege el numero binario que se esta convirtiendo
    TBA ;ahora A tiene el contenido del acumulador resultado
    ANDA #$0F ;en el nibble menos significativo de A se tienen los 4 bits correspondientes a las unidades del resultado.
    CMPA #$05
    BLO DECENAS ;si el campo de las unidades es menor que 5 se puede continuar a analizar las decenas
    ADDA #$03 ;cuando las unidades son 5 o mas se deben sumar 3 unidades.
DECENAS:
    STAA LOW ;se conservan temporalmente las unidades.
    TBA ;se vuelve a cargar en A el contenido del acumulador resultado
    ANDA #$F0 ;en el nibble mas significativo de A se tienen los 4 bits correspondientes a las decenas del resultado.
    CMPA #$50
    BLO CONFECCIONAR ;si el campo de las unidades es menor que 5 se puede continuar a analizar las decenas
    ADDA #$30 ;cuando las decenas son 5 o mas se deben sumar 3 decenas.
CONFECCIONAR:
    ADDA LOW ;se suman las unidades
    TAB ;se traslada a B el nuevo resultado parcial
    PULA ;se recupera el numero binario que se esta convirtiendo
    DEX ;se decrementa el contador de desplazamientos
    BNE OTRO_BIT ;cuando el contador no es cero significa que quedan bits por analizar.
    LSLA ;se extrae el ultimo bit
    ROLB ;se inserta el ultimo bit en el resultado final.
    TBA ;Por orden, se desea utilizar unicamente el registro A como interfaz de la subrutina.
    RTS
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina BIN_BCD: recibe como parametros de entrada las variables BIN1 y
;     BIN2 y realiza la conversion a BCD de cada una de estas variables. La
;     conversion de BIN2 solo se da en el modo RUN puesto que es el unico modo
;     en el que BIN2 tiene valores correctos. Ademas, luego de la conversion, si
;     el numero es menor que 10 significa que el display de 7 segmentos utilizado
;     para las decenas no es necesario que este encendido; en este caso se escribe
;     $F en el nibble mas significativo de BCD1 y BCD2 para indicarlo.
;------------------------------------------------------------------------------
BIN_BCD:
    LDAA BIN1 ;se carga parametro de entrada a BIN_BCD_BASE
    JSR BIN_BCD_BASE
    CMPA #10
    BHS TRF_BCD1 ;si el numero es mayor o igual a 10 no hay que apagar ninguno display
    ORAA #$F0 ;se pone $F en nibble mas significativo para indicar que el display se debe apagar.
TRF_BCD1:
    STAA BCD1 ;se guarda resultado en variable de salida
    BRSET Banderas %00010000 FIN_BIN_BCD ;en modo CONFIG (MODSEL=1) no es necesario convertir BIN2
    LDAA BIN2 ;se carga parametro de entrada a BIN_BCD_BASE
    JSR BIN_BCD_BASE
    CMPA #10
    BHS TRF_BCD2 ;si el numero es mayor o igual a 10 no hay que apagar ninguno display
    ORAA #$F0 ;se pone $F en nibble mas significativo para indicar que el display se debe apagar.
TRF_BCD2:
    STAA BCD2 ;se guarda resultado en variable de salida
FIN_BIN_BCD:
    RTS
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina BCD_7SEG: esta subrutina se encarga de tomar los valores en BCD1
;     y BCD2 y determinar el valor de DISP1, DISP2, DISP3, DISP4. Estas ultimas
;     cuatro variables son las que indican cuales segmentos de los displays se
;     deben encender para que se muestre el numero deseado. Sencillamente se
;     se analiza cada nibble de BCD1 y BCD2, y se toman decisiones a partir de
;     sus valores. En modo CONFIG (MODSEL=1) se apagan DISP1 y DISP2. Cuando
;     CPROG=0 se tiene la excepcion que se quieren desplegar ambos ceros
;------------------------------------------------------------------------------
BCD_7SEG:
    TST CPROG
    BNE CONVERSION ;cuando CPROG es cero se deben forzar al display ambos ceros.
    CLR DISP1 ;apaga displays de ACUMUL
    CLR DISP2 ;apaga displays de ACUMUL
    MOVB SEGMENT,DISP3 ;fuerza el cero en displays de CUENTA
    MOVB SEGMENT,DISP4 ;fuerza el cero en displays de CUENTA
    BRA FIN_BCD_7SEG

CONVERSION:
    LDX #SEGMENT ;direccion base de los valores para escribir en el puerto B.
    LDAB #$0F ;mascara para nibble menos significativo
    LDAA #$F0 ;mascara para nibble mas significativo
    ANDB BCD1 ;se extrae el nibble menos significativo de BCD1.
    MOVB B,X,DISP4
    ANDA BCD1 ;se extrae el nibble mas significativo de BCD1.
    CMPA #$F0
    BEQ DISP3_OFF ;cuando el nibble mas significativo es $F se debe apagar el DISP3
    LSRA
    LSRA
    LSRA
    LSRA ;se traslada el nibble mas significativo a la parte baja del byte.
    MOVB A,X,DISP3
    BRA AHORA_BCD2
DISP3_OFF:
    MOVB #$00,DISP3

AHORA_BCD2:
    BRSET Banderas %00010000 NO_BCD2
    LDAB #$0F ;mascara para nibble menos significativo
    LDAA #$F0 ;mascara para nibble mas significativo
    ANDB BCD2 ;se extrae el nibble menos significativo de BCD1.
    MOVB B,X,DISP2
    ANDA BCD2 ;se extrae el nibble mas significativo de BCD1.
    CMPA #$F0
    BEQ DISP1_OFF ;cuando el nibble mas significativo es $F se debe apagar el DISP1
    LSRA
    LSRA
    LSRA
    LSRA ;se traslada el nibble mas significativo a la parte baja del byte.
    MOVB A,X,DISP1
    BRA FIN_BCD_7SEG
DISP1_OFF:
    MOVB #$00,DISP1
    BRA FIN_BCD_7SEG

NO_BCD2:
    MOVB #$00,DISP2
    MOVB #$00,DISP1
FIN_BCD_7SEG:
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Subrutina TAREA_TECLADO: En esta subrutina se da la lectura del teclado. Aqui
;     se lee el teclado en el puerto A, se suprimen los rebotes, y se maneja la
;     situacion de tecla retenida.
;------------------------------------------------------------------------------
TAREA_TECLADO:  TST Cont_Reb
                BNE FIN_TAREA ;si el contador no ha llegado a cero no se han suprimido los rebotes.
                JSR MUX_TECLADO ;se lee el teclado en el puerto A.
                LDAA Tecla
                CMPA #$FF ;el valor $FF indica que no hay tecla presionada.
                BEQ GUARDAR ;si se toma el salto es porque es posible que haya una tecla lista para ser guardada en el arreglo. Al hacerlo solo cuando Tecla = $FF se evita la situacion de tecla retenida.
                BRSET Banderas,%00000010,CONFIRMACION ;cuando la bandera TCL_LEIDA=1 se debe confirmar que la tecla antes y despues de los rebotes es la misma
                MOVB Tecla,Tecla_IN ;se almacena el valor de la tecla antes de suprimir rebotes
                BSET Banderas %00000010 ;TCL_LEIDA<-1 para indicar que se esta esperando a que termine la supresion de rebotes.
                MOVB #10,Cont_Reb ;se inicia la cuenta de ticks para la supresion de rebotes.
FIN_TAREA:      RTS

GUARDAR:        BRCLR Banderas,%00000001,FIN_TAREA ;si TCL_LISTA=0 no hay nada que hacer
                BCLR Banderas %00000011 ;se limpian lsa banderas TCL_LISTA y TCL_LEIDA
                JSR FORMAR_ARRAY
                BRA FIN_TAREA

CONFIRMACION:   LDAA Tecla
                CMPA Tecla_IN
                BEQ LECTURA ;si la tecla antes y despues de la supresion de rebotes es igual entonces se dio una lectura correcta
                MOVB #$FF,Tecla ;se pone el valor nulo para la tecla
                MOVB #$FF,Tecla_IN ;se pone el valor nulo para la tecla
                BCLR Banderas %00000011 ;se limpian lsa banderas TCL_LISTA y TCL_LEIDA
                BRA FIN_TAREA
LECTURA:        BSET Banderas %00000001 ;TCL_LISTA=1 indica una lectura correcta del teclado.
                MOVB #10,Cont_Reb ;se inicia la cuenta de ticks para la supresion de rebotes.
                BRA FIN_TAREA
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
; Subrutina MUX_TECLADO: esta subrutina es la que se encarga de leer el teclado
;     en el puerto A. Como se habilitaron las resistencias de pull up, la parte
;     baja del puerto A (la entrada) siempre tendra valor %1111. Al colocar un
;     unico cero en alguno de los bits de la parte alta, si se presiona un boton
;     entonces se tendra un cero tambien en algun bit de la parte baja. Ambos
;     ceros en parte alta y baja definen la fila y columna de la tecla presionada.
;     Con esta informacion se calcula la posicion en el arreglo de teclas en el
;     que se encuentra el valor de la tecla presionada. El valor de la tecla se
;     devuelve en la variable Tecla. Si no se presiona nada entonces Tecla = $FF.
;------------------------------------------------------------------------------
MUX_TECLADO:    MOVB #$EF,PATRON  ;se inicializa el indice/patron
                LDAA #$F0 ;condicion de parada del loop. Luego de desplazar a PATRON cuatro veces este es el valor.
                CLRB   ;se inicaliza el offset por acumulador B
LOOP_PATRONES:  MOVB PATRON,PORTA
                BRCLR PORTA,%00000100,COLUMN2 ;si PA2=0 es porque se presiono una tecla en la tercer columna.
                BRCLR PORTA,%00000010,COLUMN1 ;si PA1=0 es porque se presiono una tecla en la segunda columna.
                BRCLR PORTA,%00000001,COLUMN0 ;si PA0=0 es porque se presiono una tecla en la primer columna.
                LSL PATRON;se desplaza hacia la izquierda el patron para avanzar de fila en el teclado
                ADDB #3 ;al avanzar de fila es neceasrio avanzar el offset los 3 espacios correspondientes a las 3 columnas
                CMPA PATRON ;se compara PATRON con $F0, la condicion de parada
                BNE LOOP_PATRONES ;si PATRON=$F0 es porque se recorrio todo el teclado y no se presiono ninguna tecla.
                MOVB #$FF,Tecla ;se carga el valor nulo para indicar que no se presiono una tecla
FIN_MUX:        RTS

COLUMN2:        INCB ;cuando la tecla esta en la tercer columna hay que incrementar el offset en 2.
COLUMN1:        INCB ;cuando la tecla esta en la segunda columna hay que incrementar el offset en 1.
COLUMN0:        LDX #Teclas
                MOVB B,X,Tecla ;se guarda en Tecla el valor encontrado por offset de acumulador.
                BRA FIN_MUX
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Subrutina FORMAR_ARRAY: recibe en la variable Tecla_IN el valor de la tecla
;     presionada que se quiere almacenar en array. Este arreglo representa un
;     numero en formato BCD. Se cuida que el arreglo no supere el tamano maximo.
;------------------------------------------------------------------------------
FORMAR_ARRAY:   LDX #Num_Array ;direccion base del arreglo a formar
                LDAA Tecla_IN ;la tecla presionada
                LDAB Cont_TCL ;la cantidad de valores ingresados hasta el momento
                CMPA #$0B
                BEQ BORRAR  ;se revisa si se desea borrar un valor
                CMPA #$0E
                BEQ CERRAR_ARRAY ;se revisa si se desea terminar de ingresar valores
                CMPB MAX_TCL  ;si no se dio niguno de los saltos anteriores es porque se trata de una tecla numerica
                BEQ FIN_FORMAR  ;se comprueba que haya espacio para valores nuevos, si no lo hay entonces termina la subrutina
                MOVB Tecla_IN,B,X ;se guarda el nuevo valor ingresado.
                INC Cont_TCL
FIN_FORMAR:     RTS

BORRAR:         CMPB #$00 ;para borrar primero es necesario que hayan datos que se puedan borrar
                BEQ FIN_FORMAR ;si no hay valores en el arreglo entonces termina la subrutina
                DECB
                MOVB #$FF,B,X ;se remplaza el ultimo valor ingresado por le valor nulo $FF.
                DEC Cont_TCL  ;se descuenta la cantidad de valores en el arreglo.
                BRA FIN_FORMAR

CERRAR_ARRAY:   CMPB #$00 ;para cerrar el arreglo es necesario que se haya ingresado correctamente al menos un valor.
                BEQ FIN_FORMAR ;si el arreglo esta vacio entonces termina la subrutina sin indicar que se cerro el arreglo
                CMPB MAX_TCL
                BEQ OK
                MOVB #$FF,B,X ;cuando el arreglo tiene menos valores que la cantidad maxima es necesario indicar el final.
OK:             BSET Banderas %00000100 ;ARRAY_OK=1 para indicar que se cerro el arreglo y no se deben leer mas teclas.
                CLR Cont_TCL ;para prepararlo para una futura lectura del teclado.
                BRA FIN_FORMAR
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina MODO_CONFIG: Esta subrutina corresponde a las operaciones necesarias
;     llevar a cabo la configuracion del sistema. Primero pone el valor adecuado
;     de los LEDS para que el usuario pueda saber el modo. Posteriormente, con
;     el uso de TAREA_TECLADO se da la lectura del valor CPROG. Una vez que el
;     usuario presiona ENTER se valida que el valor de CPROG este entre 12 y 96.
;     Si es asi entonces coloca este valor en BIN1 para que pueda ser desplegado
;     en los displays 3 y 4. Cuando el valor no es valido se borra CPROG.
;------------------------------------------------------------------------------
MODO_CONFIG:
    BRSET Banderas,%00000100,VALIDAR_CPROG ;si ARRAY_OK=1 no se espera lectura.
    JSR TAREA_TECLADO ;lectura del teclado
    MOVB CPROG,BIN1 ;conserva el valor de CPROG
    RTS ;retorna. mientras ARRAY_OK=0 se seguira este ciclo que lee el teclado

VALIDAR_CPROG:
    JSR BCD_BIN ;a partir de Num_Array se obtiene CPROG.
    LDAA #12 ;limite inferior
    LDAB #96 ;limite superior
    CMPA CPROG ;cuando 12 es mayor que CPROG
    BHI INVALID_CPROG ;se debe ignorar
    CMPB CPROG ;cuando 96 es menor que CPROG
    BLO INVALID_CPROG ;se debe ignorar
FIN_CONFIG:
    BCLR Banderas,%00000100 ;ARRAY_OK=0
    MOVB CPROG,BIN1 ;se pasa CPROG a la entrada de BIN_BCD
    RTS

INVALID_CPROG:
    MOVB #$00,CPROG ;se borra CPROG cuando no es valido.
    BRA FIN_CONFIG
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina MODO_RUN: esta subrutina se encarga de simular el conteo de
;     tornillos a 4Hz por medio del contador de ticks TIMER_CUENTA. Los tornillos
;     se cuentan en la variable CUENTA y cuando esta alcanza CPROG se enciende
;     el relay en el puerto PORTE4, ademas de incrementar ACUMUL para mostrar que
;     se termino otro paquete. No se permite que ACUMUL supere un valor de 99.
;------------------------------------------------------------------------------
MODO_RUN:
    LDAA CPROG ;carga CPROG en A para comparaciones
    CMPA CUENTA
    BEQ FIN_RUN ;cuando CUENTA = CPROG no hay nada que hacer, solo esperar a interrupcion en PH0
    TST TIMER_CUENTA
    BNE FIN_RUN ;cuando TIMER_CUENTA != 0 aun no se debe aumentar CUENTA
    MOVB VMAX,TIMER_CUENTA ;si TIMER_CUENTA=0 se vuelve a cargar su valor maximo
    INC CUENTA ;se incrementa la cuenta
    CMPA CUENTA
    BNE FIN_RUN ;si CPROG != CUENTA no hay nada que hacer
    MOVB #$04,PORTE ;cuando CPROG = CUENTA se enciende el relay.
    INC ACUMUL ;se aumenta el contador de paquetes
    LDAA #100
    CMPA ACUMUL ;se comprueba que ACUMUL no supere el valor de 99/
    BNE FIN_RUN
    CLR ACUMUL
FIN_RUN:
    MOVB CUENTA,BIN1 ;se actualizan las entradas a BIN_BCD
    MOVB ACUMUL,BIN2 ;se actualizan las entradas a BIN_BCD
    RTS
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina Delay: se mantiene en un loop cerrado hasta que Cont_Delay sea 0.
;     Cont_Delay es descontado por OC4 a 50 kHz.
;------------------------------------------------------------------------------
Delay:
    TST Cont_Delay
    BNE DELAY
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina Send_Command: se encarga de enviar al LCD el comando que recibe
;     por el acumulador A.
;------------------------------------------------------------------------------
Send_Command:
    PSHA ;el comando se recibe en acumulador A y se protege para poder analizar sus nibbles por separado
		ANDA #$F0 ;Se deja solo el nibble superior del comando a ejecutar
		LSRA
		LSRA ;se alinea nibble con bus datos en PORTK5-PORTK2.
		STAA PORTK ;se carga parte alta del comando en el bus de datos.
		BCLR PORTK,$01 ;Se habilita el envío de comandos.
		BSET PORTK,$02 ;Se habilita comunicacion con la LCD.
		MOVB D260us,Cont_Delay ;se inicia el retardo de 260us.
		JSR Delay
		BCLR PORTK,$02 ;Se deshabilita comunicacion con la LCD
		PULA ;se recupera el comando original de la pila
		ANDA #$0F ;Se deja solo el nibble inferior del comando a ejecutar
		LSLA
		LSLA ;se alinea nibble con bus datos en PORTK5-PORTK2.
		STAA PORTK ;se carga parte baja del comando en el bus de datos.
		BSET PORTK,$02 ;Se habilita comunicacion con la LCD.
		MOVB D260us,Cont_Delay ;se inicia el retardo de 260us.
		JSR Delay
		BCLR PORTK,$02 ;Se deshabilita comunicacion con la LCD
		RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina Send_Data: se encarga de enviar al LCD el dato que recibe
;     por el acumulador A.
;------------------------------------------------------------------------------
Send_Data:
    PSHA ;el dato se recibe en acumulador A y se protege para poder analizar sus nibbles por separado
		ANDA #$F0 ;Se deja solo el nibble superior del dato
		LSRA
		LSRA ;se alinea nibble con bus datos en PORTK5-PORTK2.
		STAA PORTK ;se carga parte alta del dato en el bus de datos.
		BSET PORTK,$01 ;Se habilita el envío de dato.
		BSET PORTK,$02 ;Se habilita comunicacion con la LCD.
		MOVB D260us,Cont_Delay ;se inicia el retardo de 260us.
		JSR Delay
		BCLR PORTK,$02 ;Se deshabilita comunicacion con la LCD
		PULA ;se recupera el dato original de la pila
		ANDA #$0F ;Se deja solo el nibble inferior del dato
		LSLA
		LSLA ;se alinea nibble con bus datos en PORTK5-PORTK2.
		STAA PORTK ;se carga parte baja del dato en el bus de datos.
    BSET PORTK,$01 ;Se habilita envío de datos
		BSET PORTK,$02 ;Se habilita comunicacion con la LCD.
		MOVB D260us,Cont_Delay ;se inicia el retardo de 260us.
		JSR Delay
		BCLR PORTK,$02 ;Se deshabilita comunicacion con la LCD
		RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina INIT_LCD: se encarga de inicializar la pantalla LCD ejecutando la
;     secuencia correcta de comandos.
;------------------------------------------------------------------------------
INIT_LCD:
    LDX #iniDsp+1 ;Se carga en X la tabla que contiene los comandos de inicialización. Posicion 0 tiene el tamano de la tabla.
		CLRB
COMMANDS:
    LDAA B,X ;Se recorren los comandos con direccionamiento indexado por acumulador B
		JSR Send_Command ;Se ejecuta cada comando
		MOVB D40us,Cont_Delay ;40us son necesarios luego de enviar cualquiera de los comando de inicializacion
		JSR Delay
		INCB ;siguiente comando
		CMPB iniDsp
		BNE COMMANDS ;Si ya se ejecutaron todos los comandos de la tabla, terminar comandos de inicialización
		LDAA CLEAR_LCD ;Cargar comando de limpiar pantalla
		JSR Send_Command ;enviar comando de limpiar pantalla
		MOVB D2ms,Cont_Delay ;luego de enviar comando limpiar pantalla se debe esperar 2ms
		JSR Delay
		RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina Cargar_LCD: esta subrutina se encarga de enviar a la pantalla LCD
;     cada caracter, uno por uno, de ambas lineas del LCD. Recibe los parametros
;     en los registros indice X y Y, que contienen las direcciones de inicio a
;     los mensajes de las lineas 1 y 2 respectivamente.
;------------------------------------------------------------------------------
Cargar_LCD:
    LDAA ADD_L1	;Se carga la dirección de la primera posición de la primera fila de la LCD
		JSR Send_Command ;Se ejecuta el comando
		MOVB D40uS,Cont_Delay
		JSR Delay
LINE1:
    LDAA 1,X+ ;Se carga cada caracter en A
		BEQ CARG_2 ;Si se encuentra un caracter de EOM ($00) se terminó de imprimir la primera fila
		JSR Send_Data ;Se imprime cada caracter
		MOVB D40us,Cont_Delay
		JSR Delay
		BRA LINE1
CARG_2:
    LDAA ADD_L2 ;Se carga la dirección de la primera posición de la segunda fila de la LCD
		JSR Send_Command
		MOVB D40us,Cont_Delay
		JSR Delay
LINE2:
    LDAA 1,Y+ ;Se carga cada caracter en A
		BEQ FIN_Cargar_LCD ;Si se encuentra un caracter de EOM ($00) se terminó de imprimir la primera fila
		JSR Send_Data ;Se imprime cada caracter
		MOVB D40us,Cont_Delay
		JSR Delay
		BRA LINE2
FIN_Cargar_LCD:
	  RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina de servicio a interrupcion RTI: Esta subrutina descuenta contadores
;     siempre y cuando no sean cero. Los ticks del RTI duran 1.024 ms, por lo
;     que si se cargan variables con X valor se pueden contar aproximadamente
;     X milisegundos. Cont_Reb tiene un valor maximo de 10; se utiliza para
;     suprimir rebotes contando ~10ms. TIMER_CUENTA tiene un valor maximo de
;     VMAX (245), y cuenta ~250ms; se utiliza para simular la cuenta de tornillos
;     a 4 Hz.
;------------------------------------------------------------------------------
RTI_ISR:
    BSET CRGFLG %10000000  ;reinicia la bandera de interrupcion
    TST Cont_Reb
    BEQ NEXT_RTI_ISR       ;si el contador ya esta en cero no hay nada que hacer.
    DEC Cont_Reb
NEXT_RTI_ISR:
    TST TIMER_CUENTA
    BEQ FIN_RTI_ISR ;si el contador ya esta en cero no hay nada que hacer.
    DEC TIMER_CUENTA
FIN_RTI_ISR:
    RTI
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina de servicio a interrupcion keywakeup en puerto H: revisa las 4
;     posibles fuentes de interrupcion PH0, PH1, PH2, PH3. La primer borra el
;     valor de CUENTA, la segunda borra el valor de ACUMUL, la tercera disminuye
;     el brillo en un 5%, la cuarta aumenta el brillo en un 5%.
;------------------------------------------------------------------------------
PTH_ISR:
    BRCLR PIFH %00000001 BORRAR_ACUMUL ;si PIFH.0=0 entonces se revisa la siguiente fuente de interrupcion
    BSET PIFH %00000001 ;se apaga la bandera de la fuente de interrupcion
    MOVB #$00,PORTE
    CLR CUENTA

BORRAR_ACUMUL:
    BRCLR PIFH %00000010 BAJAR_BRILLO ;si PIFH.1=0 entonces se revisa la siguiente fuente de interrupcion
    BSET PIFH %00000010 ;se apaga la bandera de la fuente de interrupcion
    CLR ACUMUL

BAJAR_BRILLO:
    LDAA BRILLO
    LDAB #5
    BRCLR PIFH %00000100 SUBIR_BRILLO ;si PIFH.2=0 entonces se revisa la siguiente fuente de interrupcion
    BSET PIFH %00000100 ;se apaga la bandera de la fuente de interrupcion
    CMPA #0
    BEQ SUBIR_BRILLO ;si BRILLO = 0 no se puede disminuir, solo aumentar.
    SBA
    STAA BRILLO

SUBIR_BRILLO:
    BRCLR PIFH %00001000 FIN_PTH_ISR ;si PIFH.3=0 se termina la subrutina porque ya no hay mas posible fuentes.
    BSET PIFH %00001000 ;se apaga la bandera de la fuente de interrupcion
    CMPA #100
    BEQ FIN_PTH_ISR ;si BRILLO = 100 no se puede aumentar, solo disminuir.
    ABA
    STAA BRILLO

FIN_PTH_ISR:
    RTI
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina de servicio a interrupcion por output compare en el canal 4:
;     Descuenta Cont_Delay, refresca cada 100 ms (5000ticks) los valores de
;     DISP1-DISP4, multiplexa el bus del puerto B para mostrar informacion en
;     los displays de 7 segmentos y los LEDS, y todo con un ciclo de trabajo
;     variable que depende de BRILLO.
;------------------------------------------------------------------------------
OC4_ISR:
    TST Cont_Delay
    BEQ REFRESH ;se descuenta el contador solo si no es cero.
    DEC Cont_Delay

REFRESH:
    LDD CONT_7SEG ;por tratarse de un WORD se debe traer al registro D para restarle 1
    SUBD #1
    STD CONT_7SEG ;se guarda el nuevo valor, y esto a la vez afecta la bandera Z.
    BNE SELECT_DISP ;cuando CONT_7SEG=0 se refrescan los valores de los displays.
    MOVW #5000,CONT_7SEG ;se reinicia el contador de refrescamiento de la informacion
    JSR BCD_7SEG ;se refresca la informacion

SELECT_DISP:
    LDAA #100 ;N
    CMPA CONT_TICKS ;cuando CONT_TICKS=N se debe cambiar de digito
    BNE MULTIPLEX ;si no es cero entonces no hay que cambiar de digito y se puede continuar
    CLR CONT_TICKS ;se reiniciar contador de ticks
    INC CONT_DIG ;se pasa al siguiente digito
    LDAA #5
    CMPA CONT_DIG ;cuando CONT_DIG alcance 5 se debe volver a colocar en 0 para que sea circular.
    BNE MULTIPLEX ;si no es 5 no hay que corregir nada y se puede continuar
    CLR CONT_DIG

MULTIPLEX:
    LDAA #100 ;N
    SUBA BRILLO ; N-K
    STAA DT ;DT=N-K
    TST CONT_TICKS
    BNE DUTY_CYCLE ;cuando CONT_TICKS=0 se debe habiliar algun Display. Si no, se puede pasar a comprobar el ciclo de trabajo
    MOVB #$02,PTJ ;se deshabilitan los LEDS
    MOVB #$FF,PTP ;se deshabilitan displays de 7 segmentos
    LDAA CONT_DIG ;se comparan todos los posibles valores para determinar cual display encender.
    CMPA #0
    BEQ DIG0
    CMPA #1
    BEQ DIG1
    CMPA #2
    BEQ DIG2
    CMPA #3
    BEQ DIG3
;Ningun Display se debe habilitar, entonces son los LEDS.
    MOVB #$00,PTJ ;se habilitan los LEDS
    MOVB LEDS,PORTB ;se coloca en puerto B el estado de los LEDS.
    BRA DUTY_CYCLE ;se pasa a comprobar el ciclo de trabajo

DIG0:
    MOVB #$F7,PTP ;se habilita unicamente el display 4
    MOVB DISP4,PORTB ;se coloca en el puerto B el valor del display 4
    BRA DUTY_CYCLE

DIG1:
    MOVB #$FB,PTP ;se habilita unicamente el display 3
    MOVB DISP3,PORTB ;se coloca en el puerto B el valor del display 3
    BRA DUTY_CYCLE

DIG2:
    MOVB #$FD,PTP ;se habilita unicamente el display 2
    MOVB DISP2,PORTB ;se coloca en el puerto B el valor del display 2
    BRA DUTY_CYCLE

DIG3:
    MOVB #$FE,PTP ;se habilita unicamente el display 1
    MOVB DISP1,PORTB ;se coloca en el puerto B el valor del display 1

DUTY_CYCLE:
    LDAA CONT_TICKS
    CMPA DT
    BNE FIN_OC4_ISR
    MOVB #$FF,PTP ;se deshabilitan displays de 7 segmentos
    MOVB #$02,PTJ ;se deshabilitan los LEDS

FIN_OC4_ISR:
    INC CONT_TICKS
    BSET TFLG1 %00010000 ;se reinicia la bandera de interrupcion
    LDD TCNT ;se carga el valor actual de TCNT para reajustar el output compare
    ADDD #60 ;60 cuentas equivalen 50kHz con prescalador=8
    STD TC4 ;se actualiza el nuevo valor a alcanzar.
    RTI
;------------------------------------------------------------------------------
