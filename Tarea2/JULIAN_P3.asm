;##############################################################################
;                             Programa Divisibles4
;##############################################################################
;       Fecha: 27 de setiembre del 2019
;       Autor: Julian Morua Vindas
;
; Descripcion: Este programa recorre un arreglo de numeros y copia a otro arreglo
; unicamente los numeros que son divisibles por 4.
;##############################################################################


;------------------------------------------------------------------------------
;                     Declaracion de estructuras de datos
;------------------------------------------------------------------------------
                ORG $1000
L:                DS 1                ;Variable tipo byte con la cantidad de numeros a analizar
CANT4:          DS 1            ;Variable tipo byte con la cantidad de numeros divisbles por 4

                ORG $1100         ;Direccion inicial del arreglo de datos.
DATOS:          DS 254

                ORG $1200         ;Direccion inicial del arreglo de resultados divisibles por 4.
DIV4:               DS 254

;------------------------------------------------------------------------------
;                                Programa principal
;------------------------------------------------------------------------------

                ORG $1300
;Primero, inicializamos variables de conteo e indices:
                CLRA
                CLR CANT4
                LDX #DATOS
                LDY #DIV4
;Ahora podemos recorrer el arreglo DATOS con indexamiento por offset en acumulador A:
LOOP:                  LDAB A,X
                LSRB
                BCS ITERAR
                       LSRB
                BCS ITERAR
;Si no se toman ninguno de los dos branches, el numero es divisible por 4. Se copia a DIV4 con indexamiento por offset en acumulador B
                LDAB CANT4
                MOVB A,X, B,Y
                INC CANT4
;Por ultimo, se avanza el iterador y se comprueba que se hayan analizado todos los datos.
ITERAR:         INCA
                CMPA L
                BNE LOOP
FIN:            BRA FIN
