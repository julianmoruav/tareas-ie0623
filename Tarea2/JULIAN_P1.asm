;##############################################################################
;                             Programa Ordenamiento
;##############################################################################
;       Fecha: 27 de setiembre del 2019
;       Autor: Julian Morua Vindas
;
; Descripcion: este programa ordena los datos de un arreglo de menor a mayor con el algoritmo Bubblesort y
; posteriomente copia los datos en otra posicion de memoria sin los elementos repetidos.
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;                     Declaracion de estructuras de datos
;------------------------------------------------------------------------------
                ORG $1000 ;direccion de variable CANT
CANT:           DS 1 ;variable tipo bye con la cantidad de datos. Son maximo 199 datos
ENDING:         DS 2 ;variable tipo word con la direccion del final del arreglo ORDENAR.

                ORG $1100 ;direccion del arreglo a ordenar
ORDENAR:        DS 199

                ORG $1200 ;direccion del arreglo ordenado
ORDENADOS:        DS 199

;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;                                Programa principal
;------------------------------------------------------------------------------
                ORG $1500
; Primero vamos a calcular el valor de ENDING:
                LDAB CANT
                ADDD #ORDENAR   ;se suma al tamano la direccion base para obtener direccion del final.
                SUBD #1         ;se resta uno porque el 0 es una posicion incluida.
                STD ENDING      ;se almacena la direccion final en el espacio previsto.

; Ahora se implementa el algoritmo Bubble Sort para el ordenamiento:
                LDAA CANT       ;para iterar
LOOP_EXT:       LDX #ORDENAR
LOOP_INT:       LDAB 0,X        ;se carga un primer dato en B
                CMPB 1,+X       ;el dato en B se compara con el siguiente dato consecutivo. Note que esto incrementa el indice J.
                BLT NO_SWAP     ;Si en los datos consecutivos el primero es menor entonces no hay que intercambiarlos, pues estan ordenados.
                MOVB 0,X, -1,X  ;se pasa el segundo dato (el menor) a la posicion del primer dato (el mayor).
                STAB 0,X        ;se guarda el primer dato (el mayor) en la posicion del segundo dato (el menor).
NO_SWAP:        CPX ENDING      ;se compara el indice con la direccion del final del arreglo
                BNE LOOP_INT    ;si no es el final del arreglo entonces hay que seguir haciendo comparaciones consecutivas.
                DBNE A,LOOP_EXT ;se debe iterar CANT veces.

; Hasta este punto se tiene el arreglo ordenado de menor a mayor con numeros repetidos.
; Ahora se debe copiar a la direccion destino sin los repetidos.

                LDX #ORDENAR
                LDY #ORDENADOS
                LDAA CANT 	;para iterar
COPIAR:         MOVB 0,X,1,Y+ 	;no se incrementa X con direccionamiento de post incremento porque se quiso separar-
NO_COPIAR:      INX        	; -el incremento de forma que se puediera hacer independientemente de si se va a copiar o no.
                DBEQ A,FIN 	;al iterar CANT veces se copiaron todos los datos y el prgrama termina
                LDAB 0,X
                CMPB -1,X
                BNE COPIAR  	;si los datos consecutivos no son iguales entonces hay que copiarlos
                JMP NO_COPIAR   ;cuando los datos consecutivos son iguales solo se avanza el indice X
FIN:            BRA FIN
