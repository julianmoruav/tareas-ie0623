;##############################################################################
;                             Programa Enmascarar
;##############################################################################
;       Fecha: 27 de setiembre del 2019
;       Autor: Julian Morua Vindas
;
; Descripcion: Este programa recorre una tabla de datos de fin a inicio mientras
; recorre una tabla de mascaras de inicio a fin, y realiza la operacion XOR entre
; el dato y la mascara de cada iteracion en el recorrido. Copia los resultados
; negativos en un tercer arreglo. El programa termina cuando procesa todos los
; contenidos de alguna de las tablas.
;##############################################################################


;------------------------------------------------------------------------------
;                     Declaracion de estructuras de datos
;------------------------------------------------------------------------------

                ORG $1050 ;Direccion inicial de la tabla de datos.
DATOS:          DB $80, $04, $01, $FE, $00, $42, $FF ;el ultimo dato siempre es $FF

                ORG $1150 ;Direccion inicial de la tabla de mascaras.
MASCARAS:       DB $7F, $80, $01, $EF, $FE  ;la ultima mascara siempre es $FE

                ORG $1300 ;Direccion inicial del arreglo de resultados negativos.
NEGAT:          DS 999 ;se reservan 999 bytes porque los arreglos porque se indica que las tablas pueden ser de tamano menor a 1000.

                ORG $1700
OFFSET:         DS 2 ;variable tipo word con el offset que existe entre la tabla MASCARAS y el arreglo NEGAT. Su uso permite utilizar el mismo registro indice para recorrer ambos arreglos.
TEMP:           DS 1 ;variable tipo byte que almacena temporalmente el resultado de la operacion XOR para facilitar el movimiento del dato al arreglo NEGAT.

;------------------------------------------------------------------------------
;                                Programa principal
;------------------------------------------------------------------------------

                ORG $2000
; Se obtiene el OFFSET entre las direcciones de inicio de la tabla MASCARAS y el arreglo NEGAT:
                LDD #NEGAT-MASCARAS ;ambos valores estan definidos en tiempo de ensamblado por lo que es valida la operacion.
                STD OFFSET
; Avanzamos el indice X hasta al final del arreglo DATOS:
                LDAA #$FF         ;se sabe que el final del arreglo contiene este dato.
                LDX #DATOS
AVANZAR:        INX
                CMPA 0,X
                BNE AVANZAR
; Ahora se procede a recorrer los arreglos DATOS (del final al inicio) y MASCARAS (del inicio al final).
                LDY #MASCARAS
ITERAR:         LDAA 0,X
                EORA 0,Y         ;aqui se aplica el XOR, el resultado afecta la bander N.
                BMI NEGATIVO         ;la instruccion BMI revisa si N = 1, lo que indica que el numero resultado es negativo.
                LDD OFFSET         ;no hay instruccion de decremento a un word en memoria, entonces es necesario cargarlo a D.
                SUBD #1            ;se resta 1 al OFFSET
                STD OFFSET         ;se vuelve a almacenar el OFFSET decrementado.
                JMP CONDICIONES
;Si no se reduce el offset habrian en medio de NEGAT espacios con basura porque el indice Y siempre se incremena, pero no siempre se debe copiar a NEGAT.
NEGATIVO:       STAA TEMP
                LDD OFFSET         ;se carga la separacion que existe entre el puntero de lectura de MASCARAS y el de escritura de NEGAT.
                MOVB TEMP, D,Y         ;se copia el dato con offset por acumulador.
CONDICIONES:    CPX #DATOS         ;se compara que el indice X con la direccion de inicio del arreglo DATOS.
                BEQ FIN         ;si son iguales entonces no hay mas datos por enmascarar y el programa termina.
                LDAA #$FE       ;se sabe que esta es la ultima mascara en la tabla MASCARA.
                CMPA 0,Y
                BEQ FIN         ;si la ultima mascara utilizada es $FE entonces no hay mas mascaras que utilizar y el programa termina.
                DEX             ;el indice X avanza un espacio hacia el inicio de la tabla DATOS.
                INY             ;el indice Y abanza un espacio hacia el final de la tabla MASCARAS.
                JMP ITERAR
FIN:            BRA FIN
