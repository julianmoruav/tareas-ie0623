;##############################################################################
;                        Tarea 7: Reloj despertador
;   Fecha: 9 de diciembre del 2019.
;   Autor: Julian Morua Vindas
;
;   Descripcion: esta tarea implementa un reloj despertado que por defecto configura
;     la hora a las 8 horas, 00 minutos y 00 segundos del miercoles 4 de diciembre
;     del 2019; esta es la hora de entrega. La alarma esta configurada a las 8
;     y 1 minuto, por lo que luego de un minuto de haber corrido el programa
;     sonara un tono agudo. El boton PH1 apaga esta alarma. El boton PH0 reinicia
;     recarga la hora programada del reloj y ademas vuelve a programar la alarma.
;     Los botones PH2 y PH3 ajustan el brillo.
;##############################################################################
#include registers.inc
SET_ALARM: EQU $0108 ;formato MM:HH

;------------------------------------------------------------------------------
;     Declaracion de las estructuras de datos y vectores de interrupcion
;------------------------------------------------------------------------------
;Vectores de interrupcion:
               ORG $3E70   ;direccion del vector de interrupcion RTI.
               DW RTI_ISR  ;direccion de la subrutina de servicio a interrupcion RTI.
               ORG $3E4C   ;direccion del vector de interrupcion por key wakeup del puerto H.
               DW PTH_ISR  ;direccion de la subrutina de servicio a interrupcion del puerto H.
               ORG $3E66   ;direccion del vector de interrupcion OC4.
               DW OC4_ISR  ;direccion de la subrutina de servicio a interrupcion OC4.
               ORG $3E64   ;direccion del vector de interrupcion OC5.
               DW OC5_ISR  ;direccion de la subrutina de servicio a interrupcion OC5.
               ORG $3E40   ;direccion del vector de interrupcion IIC.
           		 DW IIC_ISR  ;direccion de la subrutina de servicio a interrupcion IIC.


;Estructuras de datos:
                ORG $1000
CONT_RTI:       DS 1
BANDERAS:       DS 1  ;Es un word donde cada bit representa una bandera que se explica a continuacion:
                      ;bit 0: RW_RTC cuando es 1 indica lectura del IIC
                      ;bit 1: SET_ALARM se pone en uno cuando se quiere la alarma activa.

BRILLO:         DS 1
CONT_DIG:       DS 1  ;cuenta cual digito de 7 segmentos se debe habilitar. Cambia cada vez que CONT_TICKS alcanza 100.
CONT_TICKS:     DS 1  ;contador de ticks de Output Compare para multiplexar.
DT:             DS 1  ;ciclo de trabajo. DT = N-K.
BCD1:           DS 1  ;variable de salida de subrutina CONV_BIN_BCD. Entrada para BCD_7SEG.
BCD2:           DS 1  ;variable de salida de subrutina CONV_BIN_BCD. Tambien es entrada para BCD_7SEG.
DISP1:          DS 1  ;corresponde al valor que se escribe en el display de 7 segmentos.
DISP2:          DS 1  ;BCD2 utiliza DISP1 y DISP2 para desplegarse
DISP3:          DS 1  ;corresponde al valor que se escribe en el display de 7 segmentos.
DISP4:          DS 1  ;BCD1 utiliza DISP3 y DISP4 para desplegarse
LEDS:           DS 1  ;guarda el estado de los LEDS. LED PB1 corresponde a modo CONFIG, LED PB0 a modo RUN.

SEGMENT:    	  DB $3F,$06,$5B,$4F,$66,$6D,$7D,$07,$7F,$6F ;patrones para el display de 7 segmentos.
CONT_7SEG:      DS 2  ;contador de ticks de OC4 para lograr refrescamiento de LEDS y Displays a 10 Hz.
Cont_Delay:     DS 1  ;esta variable se carga con alguna de las siguientes tres constantes para generar retrasos temporales.
D2ms:           DB 100  ;100 ticks a 50kHz son 2 milisegundos
D260us:         DB 13   ;13 ticks a 50kHz son 260 microsegundos
D40us:          DB 2    ;2 ticks a 50kHz son 40 microsegundos
CLEAR_LCD:      DB $01  ;comando para limpiar el LCD
ADD_L1:         DB $80  ;direccion inicio de linea 1
ADD_L2:         DB $C0  ;direccion inicio de linea 2


FUNCTION_SET:   EQU $28
ENTRY_MODE_SET: EQU $06
DISPLAY_ON:     EQU $0C
EOM:            EQU $00
iniDsp:         DB 4,FUNCTION_SET,FUNCTION_SET,ENTRY_MODE_SET,DISPLAY_ON

INDEX_RTC:      DS 1
DIR_WR:         DB $D0
DIR_RD:         DB $D1
DIR_SEG:        DB $00
ALARMA:         DW SET_ALARM

T_WRITE_RTC:    DB $00,$00,$08,$04,$04,$12,$19
T_READ_RTC:     DS 7


                ORG $1050 ;mensajes
MSJ1:           FCC "     RELOJ"
                DB EOM
MSJ2:           FCC " DESPERTADOR 623"
                DB EOM
;------------------------------------------------------------------------------



;*******************************************************************************
;                             Programa principal
;*******************************************************************************
    ORG $2000
    LDS #$3BFF  ;inicializa el stack
    JSR INITIALIZE
    LDX #MSJ1
    LDY #MSJ2
    JSR Cargar_LCD

MAIN_LOOP:
    LDD T_READ_RTC+1 ;carga minutos:horas
    CPD ALARMA
    BEQ ES_LA_HORA
NO_ES_LA_HORA:
    BRCLR BANDERAS %00000010 RECONFIGURAR  ;si SET_ALARM = 0 y no es la hora entonces se debe poner la alarma.
    BRA MAIN_LOOP ; si SET_ALARM = 1 entonces la alarma no ha sido apagada por el usuario.

ES_LA_HORA:
    BRCLR BANDERAS %00000010 MAIN_LOOP  ;si SET_ALARM = 0 es porque ya se desactivo y no debe sonar mas.
    BSET TIE %00100000 ;se habilita interrupcion de canal 5 para activar la alarma
    BSET TCTL1 %00000100 ;pone el toggle de la salida en el canal 5 para que suene el tono
    BRA MAIN_LOOP

RECONFIGURAR:
    BSET BANDERAS %00000010 ; SET_ALARM <-- 1
    BRA MAIN_LOOP
;*******************************************************************************


;------------------------------------------------------------------------------
;   Subrutina INITIALIZE: inicializacion de HW y variables de programa.
;------------------------------------------------------------------------------
INITIALIZE:
;--------------------------
;Configuracion del hardware
;--------------------------
;Habilita interrupciones mascarables:
    CLI

;Configuracion RTI:
    BSET CRGINT %10000000 ;se habilita RTI
    MOVB #$6B,RTICTL      ;periodo de 0.0491 s ~ 50 ms

;Configuracion keywakeup en puerto H:
    BCLR PPSH %00001111   ;las interrupciones deben ocurrir en el flanco decreciente.
    BSET PIEH %00001111   ;se habilita keywakeup en PH0, PH1, PH2 y PH3.

;Configuracion del modulo de Timer (OC4 y OC5):
    BSET TSCR1 %10000000 ;se habilita modulo de timer.
    BSET TSCR2 %00000011 ;prescaler es 2^3 = 8.
    BSET TIOS %00110000 ;se configuran canales 4 y 5 como Output Compare.
    BSET TIE %00010000 ;se habilita interrupcion de canal 4. El canal 5 se habilita con la alarma.
    BCLR TCTL1 %00001111 ;se desconectan las salidas de los canales 4 y 5.

;Configuracion de los displays de 7 segmentos y los LEDS.
    MOVB #$FF,DDRB ;puerto core B se configura como salida de proposito general. (LEDS y SEGMENTOS)
    MOVB #$0F,DDRP ;parte baja de puerto P se configura como salida de proposito general. (~Habilitador Segmentos)
    BSET DDRJ %00000010 ;se configura bit 1 del puerto J como salida de proposito general . (~Habilitador LEDS)

;Configuración de pantalla LCD
    MOVB #$FF,DDRK ;todos los pines del puerto K se configura como salida para controlar la LCD.
    JSR INIT_LCD

;Configuración de IIC:
		MOVB #$25,IBFD ;Baud rate:
                   ;BUSCLK/IIC_freq = 24M/75k = 320 --> SCL Divider.
                   ;Para este SCL Divider estan: IBFD = 25, 28, 5D, 60, 94, 98.
                   ;Respectivamente los SDA hold time = 49, 33, 50, 28, 68, 36.
                   ;@24Mhz los hold times en seg son = 2.04u, 1.375u, 2.08u, 1.167u, 2.83u, 1.5u
                   ;El DS1307 tiene un SDA hold time minimo de 300 ns = 0.3us
                   ;Cualquiera sirve y se escoge el primero.
		MOVB #$D0,IBCR ;Habilita bus IIC, habilita interrpcion cuando IBIF=1, Modo esclavo para empezar, Modo TX

;---------------------------
;Inicializacion de variables
;---------------------------

;Displays de 7 segmentos y LCD:
    MOVW #1,CONT_7SEG ;se carga con 1 porque se descuenta al entrar a OC4_ISR
    CLR CONT_TICKS
    CLR CONT_DIG
    CLR BCD1
    CLR BCD2

;Programa:
    MOVB #1,CONT_RTI
    CLR BANDERAS
    MOVB #8,INDEX_RTC
    MOVB #50,BRILLO
    CLR LEDS

    ;se configura automaticamente la hora en el encendido
    BCLR BANDERAS %00000001 ; RW_RTC <-- 0 indica escritura
    BSET IBCR %00100000 ;MS/SL <-- 1 (start signal)
    MOVB DIR_WR,IBDR ;slave write address en registro de datos

;Retornar:
    RTS
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina BCD_7SEG: esta subrutina se encarga de tomar los valores en BCD1
;     y BCD2 y determinar el valor de DISP1, DISP2, DISP3, DISP4. Estas ultimas
;     cuatro variables son las que indican cuales segmentos de los displays se
;     deben encender para que se muestre el numero deseado. Sencillamente se
;     se analiza cada nibble de BCD1 y BCD2, y se toman decisiones a partir de
;     sus valores. Tambien se hace toggle de los puntos decimales en DISP2 y DISP3
;     para dar nocion de los segundos.
;------------------------------------------------------------------------------
BCD_7SEG:
    LDX #SEGMENT ;direccion base de los valores para escribir en el puerto B.

    LDAB #$0F ;mascara para nibble menos significativo
    ANDB BCD1 ;se extrae el nibble menos significativo de BCD1.
    MOVB B,X,DISP4

    LDAA #$F0 ;mascara para nibble mas significativo
    ANDA BCD1 ;se extrae el nibble mas significativo de BCD1.
    LSRA
    LSRA
    LSRA
    LSRA ;se traslada el nibble mas significativo a la parte baja del byte.
    MOVB A,X,DISP3

    LDAB #$0F ;mascara para nibble menos significativo
    ANDB BCD2 ;se extrae el nibble menos significativo de BCD1.
    MOVB B,X,DISP2

    LDAA #$F0 ;mascara para nibble mas significativo
    ANDA BCD2 ;se extrae el nibble mas significativo de BCD1.
    LSRA
    LSRA
    LSRA
    LSRA ;se traslada el nibble mas significativo a la parte baja del byte.
    MOVB A,X,DISP1

    BRCLR T_READ_RTC,$01,BLINK_ON ;se ven las unidades de los segundos para togglear los puntos
		BCLR DISP2,$80
		BCLR DISP3,$80
    RTS
BLINK_ON:
    BSET DISP2,$80
		BSET DISP3,$80
    RTS
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina Delay: se mantiene en un loop cerrado hasta que Cont_Delay sea 0.
;     Cont_Delay es descontado por OC4 a 50 kHz.
;------------------------------------------------------------------------------
Delay:
    TST Cont_Delay
    BNE DELAY
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina Send_Command: se encarga de enviar al LCD el comando que recibe
;     por el acumulador A.
;------------------------------------------------------------------------------
Send_Command:
    PSHA ;el comando se recibe en acumulador A y se protege para poder analizar sus nibbles por separado
    ANDA #$F0 ;Se deja solo el nibble superior del comando a ejecutar
    LSRA
    LSRA ;se alinea nibble con bus datos en PORTK5-PORTK2.
    STAA PORTK ;se carga parte alta del comando en el bus de datos.
    BCLR PORTK,$01 ;Se habilita el envío de comandos.
    BSET PORTK,$02 ;Se habilita comunicacion con la LCD.
    MOVB D260us,Cont_Delay ;se inicia el retardo de 260us.
    JSR Delay
    BCLR PORTK,$02 ;Se deshabilita comunicacion con la LCD
    PULA ;se recupera el comando original de la pila
    ANDA #$0F ;Se deja solo el nibble inferior del comando a ejecutar
    LSLA
    LSLA ;se alinea nibble con bus datos en PORTK5-PORTK2.
    STAA PORTK ;se carga parte baja del comando en el bus de datos.
    BSET PORTK,$02 ;Se habilita comunicacion con la LCD.
    MOVB D260us,Cont_Delay ;se inicia el retardo de 260us.
    JSR Delay
    BCLR PORTK,$02 ;Se deshabilita comunicacion con la LCD
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina Send_Data: se encarga de enviar al LCD el dato que recibe
;     por el acumulador A.
;------------------------------------------------------------------------------
Send_Data:
    PSHA ;el dato se recibe en acumulador A y se protege para poder analizar sus nibbles por separado
    ANDA #$F0 ;Se deja solo el nibble superior del dato
    LSRA
    LSRA ;se alinea nibble con bus datos en PORTK5-PORTK2.
    STAA PORTK ;se carga parte alta del dato en el bus de datos.
    BSET PORTK,$01 ;Se habilita el envío de dato.
    BSET PORTK,$02 ;Se habilita comunicacion con la LCD.
    MOVB D260us,Cont_Delay ;se inicia el retardo de 260us.
    JSR Delay
    BCLR PORTK,$02 ;Se deshabilita comunicacion con la LCD
    PULA ;se recupera el dato original de la pila
    ANDA #$0F ;Se deja solo el nibble inferior del dato
    LSLA
    LSLA ;se alinea nibble con bus datos en PORTK5-PORTK2.
    STAA PORTK ;se carga parte baja del dato en el bus de datos.
    BSET PORTK,$01 ;Se habilita envío de datos
    BSET PORTK,$02 ;Se habilita comunicacion con la LCD.
    MOVB D260us,Cont_Delay ;se inicia el retardo de 260us.
    JSR Delay
    BCLR PORTK,$02 ;Se deshabilita comunicacion con la LCD
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina INIT_LCD: se encarga de inicializar la pantalla LCD ejecutando la
;     secuencia correcta de comandos.
;------------------------------------------------------------------------------
INIT_LCD:
    LDX #iniDsp+1 ;Se carga en X la tabla que contiene los comandos de inicialización. Posicion 0 tiene el tamano de la tabla.
    CLRB
COMMANDS:
    LDAA B,X ;Se recorren los comandos con direccionamiento indexado por acumulador B
    JSR Send_Command ;Se ejecuta cada comando
    MOVB D40us,Cont_Delay ;40us son necesarios luego de enviar cualquiera de los comando de inicializacion
    JSR Delay
    INCB ;siguiente comando
    CMPB iniDsp
    BNE COMMANDS ;Si ya se ejecutaron todos los comandos de la tabla, terminar comandos de inicialización
    LDAA CLEAR_LCD ;Cargar comando de limpiar pantalla
    JSR Send_Command ;enviar comando de limpiar pantalla
    MOVB D2ms,Cont_Delay ;luego de enviar comando limpiar pantalla se debe esperar 2ms
    JSR Delay
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina Cargar_LCD: esta subrutina se encarga de enviar a la pantalla LCD
;     cada caracter, uno por uno, de ambas lineas del LCD. Recibe los parametros
;     en los registros indice X y Y, que contienen las direcciones de inicio a
;     los mensajes de las lineas 1 y 2 respectivamente.
;------------------------------------------------------------------------------
Cargar_LCD:
    LDAA ADD_L1	;Se carga la dirección de la primera posición de la primera fila de la LCD
		JSR Send_Command ;Se ejecuta el comando
		MOVB D40uS,Cont_Delay
		JSR Delay
LINE1:
    LDAA 1,X+ ;Se carga cada caracter en A
		BEQ CARG_2 ;Si se encuentra un caracter de EOM ($00) se terminó de imprimir la primera fila
		JSR Send_Data ;Se imprime cada caracter
		MOVB D40us,Cont_Delay
		JSR Delay
		BRA LINE1
CARG_2:
    LDAA ADD_L2 ;Se carga la dirección de la primera posición de la segunda fila de la LCD
		JSR Send_Command
		MOVB D40us,Cont_Delay
		JSR Delay
LINE2:
    LDAA 1,Y+ ;Se carga cada caracter en A
		BEQ FIN_Cargar_LCD ;Si se encuentra un caracter de EOM ($00) se terminó de imprimir la primera fila
		JSR Send_Data ;Se imprime cada caracter
		MOVB D40us,Cont_Delay
		JSR Delay
		BRA LINE2
FIN_Cargar_LCD:
	  RTS
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina de servicio a interrupcion RTI: interrumpe con un periodo de 1 seg
;     aproximadamente. En cada segundo se da la lectura del modulo RTC para
;     actualizar la hora y ademas se refrescan los displays.
;------------------------------------------------------------------------------
RTI_ISR:
    DEC CONT_RTI
    BNE FIN_RTI_ISR
    MOVB #20,CONT_RTI
    BSET BANDERAS %00000001 ; RW_RTC <-- 1 indica lectura
    BSET IBCR %00100000 ;MS/SL <-- 1 (start signal)
    MOVB DIR_WR,IBDR ;slave write address en registro de datos
    MOVB T_READ_RTC+1,BCD1 ;minutos
    MOVB T_READ_RTC+2,BCD2 ;horas

FIN_RTI_ISR:
    BSET CRGFLG %10000000  ;reinicia la bandera de interrupcion
    RTI
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina de servicio a interrupcion keywakeup en puerto H:
;     PH0 reajusta el modulo RTC a la hora programada. PH1 apaga la alarma cuando
;     esta está sonando. PH2 y PH3 suben y bajan el brillo de los displays.
;------------------------------------------------------------------------------
PTH_ISR:
    BRSET PIFH %00000001 AJUSTAR
    BRSET PIFH %00000010 APAGAR
    LDAA BRILLO
    LDAB #5 ;saltos de 5 en 5 para lograr 20 intensidades
    BRSET PIFH %00000100 BAJAR_BRILLO
    BRSET PIFH %00001000 SUBIR_BRILLO
FIN_PTH_ISR:
    RTI

AJUSTAR:
    BCLR BANDERAS %00000001 ; RW_RTC <-- 0 indica escritura
    BSET IBCR %00100000 ;MS/SL <-- 1 (start signal)
    MOVB DIR_WR,IBDR ;slave write address en registro de datos
    BSET PIFH %00000001 ;apaga fuente de interrupcion
    BRA FIN_PTH_ISR

APAGAR:
    BRCLR TIE %00100000 NO_APAGAR
    BCLR BANDERAS %00000010 ; SET_ALARM <-- 0
    BCLR TIE %00100000 ;apaga interrupcion del canal OC5 para apagar la alarma.
    BCLR TCTL1 %00000100 ;quita el toggle de la salida en el canal 5 para que no suene el tono
NO_APAGAR:
    BSET PIFH %00000010 ;apaga fuente de interrupcion
    BRA FIN_PTH_ISR

BAJAR_BRILLO:
    BSET PIFH %00000100 ;apaga fuente de interrupcion
    CMPA #0
    BEQ FIN_PTH_ISR
    SBA
    STAA BRILLO
    BRA FIN_PTH_ISR

SUBIR_BRILLO:
    BSET PIFH %00001000 ;apaga fuente de interrupcion
    CMPA #100
    BEQ FIN_PTH_ISR
    ABA
    STAA BRILLO
    BRA FIN_PTH_ISR
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina de servicio a interrupcion por output compare en el canal 5: genera
;      un tono en el buzzer haciendo toggle de la salida OC5 a 440 Hz.
;------------------------------------------------------------------------------
OC5_ISR:
    LDD TCNT
    ADDD #682 ;para que interrumpa a 440 Hz (Nota musical A4, LA)
    STD TC5
    BSET TFLG1 %00100000 ;se reinicia la bandera de interrupcion
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina de servicio a interrupcion por output compare en el canal 4:
;     Descuenta Cont_Delay, refresca cada 100 ms (5000ticks) los valores de
;     DISP1-DISP4, multiplexa el bus del puerto B para mostrar informacion en
;     los displays de 7 segmentos y los LEDS, y todo con un ciclo de trabajo
;     variable que depende de BRILLO. OC4 interrumpe a 50 kHz (60 ticks)
;------------------------------------------------------------------------------
OC4_ISR:
    LDD CONT_7SEG ;por tratarse de un WORD se debe traer al registro D para restarle 1
    SUBD #1
    STD CONT_7SEG ;se guarda el nuevo valor, y esto a la vez afecta la bandera Z.
    BNE DEC_DELAY ;cuando CONT_7SEG=0 se refrescan los valores de los displays.
    MOVW #5000,CONT_7SEG ;se reinicia el contador de refrescamiento de la informacion
    JSR BCD_7SEG ;se refresca la informacion

DEC_DELAY:
    TST Cont_Delay
    BEQ SELECT_DISP ;se descuenta el contador solo si no es cero.
    DEC Cont_Delay

SELECT_DISP:
    LDAB BRILLO ;K
    LDAA #100 ;N
    SBA ; N-K
    STAA DT ;DT=N-K

    LDAA #100 ;N
    CMPA CONT_TICKS ;cuando CONT_TICKS=N=100 se debe cambiar de digito
    BNE MULTIPLEX ;si no es cero entonces no hay que cambiar de digito y se puede continuar
    CLR CONT_TICKS ;se reiniciar contador de ticks
    INC CONT_DIG ;se pasa al siguiente digito
    LDAA #5
    CMPA CONT_DIG ;cuando CONT_DIG alcance 5 se debe volver a colocar en 0 para que sea circular.
    BNE MULTIPLEX ;si no es 5 no hay que corregir nada y se puede continuar
    CLR CONT_DIG

MULTIPLEX:
    TST CONT_TICKS
    BNE DUTY_CYCLE ;cuando CONT_TICKS=0 se debe habiliar algun Display. Si no, se puede pasar a comprobar el ciclo de trabajo
    MOVB #$02,PTJ ;se deshabilitan los LEDS
    MOVB #$FF,PTP ;se deshabilitan displays de 7 segmentos
    LDAA CONT_DIG ;se comparan todos los posibles valores para determinar cual display encender.
    CMPA #0
    BEQ DIG0
    CMPA #1
    BEQ DIG1
    CMPA #2
    BEQ DIG2
    CMPA #3
    BEQ DIG3
;Ningun Display se debe habilitar, entonces son los LEDS.
    MOVB #$00,PTJ ;se habilitan los LEDS
    MOVB LEDS,PORTB ;se coloca en puerto B el estado de los LEDS.
    BRA DUTY_CYCLE ;se pasa a comprobar el ciclo de trabajo

DIG0:
    MOVB #$F7,PTP ;se habilita unicamente el display 4
    MOVB DISP4,PORTB ;se coloca en el puerto B el valor del display 4
    BRA DUTY_CYCLE

DIG1:
    MOVB #$FB,PTP ;se habilita unicamente el display 3
    MOVB DISP3,PORTB ;se coloca en el puerto B el valor del display 3
    BRA DUTY_CYCLE

DIG2:
    MOVB #$FD,PTP ;se habilita unicamente el display 2
    MOVB DISP2,PORTB ;se coloca en el puerto B el valor del display 2
    BRA DUTY_CYCLE

DIG3:
    MOVB #$FE,PTP ;se habilita unicamente el display 1
    MOVB DISP1,PORTB ;se coloca en el puerto B el valor del display 1

DUTY_CYCLE:
    LDAA CONT_TICKS
    CMPA DT
    BNE FIN_OC4_ISR
    MOVB #$FF,PTP ;se deshabilitan displays de 7 segmentos
    MOVB #$02,PTJ ;se deshabilitan los LEDS

FIN_OC4_ISR:
    INC CONT_TICKS
    BSET TFLG1 %00010000 ;se reinicia la bandera de interrupcion
    LDD TCNT ;se carga el valor actual de TCNT para reajustar el output compare
    ADDD #60 ;60 cuentas equivalen 50kHz con prescalador=8
    STD TC4 ;se actualiza el nuevo valor a alcanzar.
    RTI
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina IIC_ISR: determina si se debe hacer uso de la interfaz IIC para
;     escribir o leer del modulo RTC.
;------------------------------------------------------------------------------
IIC_ISR:
    BSET IBSR %00000010 ;reinicia fuente de interrupcion
    BRSET BANDERAS %00000001 READ_IIC;si RW_RTC = 1 es lectura
    JSR WRITE_RTC
    BRA FIN_IIC_ISR
READ_IIC:
    JSR READ_RTC
FIN_IIC_ISR:
    RTI
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina WRITE_RTC: se encarga de hacer la escritura al modulo RTC por medio
;     de la interfaz IIC.
;------------------------------------------------------------------------------
WRITE_RTC:
    LDAA INDEX_RTC
    CMPA #8 ;primera interrupcion
    BEQ PRIMERA_WR
    CMPA #7 ;ultima interrupcion
    BEQ STOP_WR
    LDX #T_WRITE_RTC ;direccion base del contenido a escribir
    MOVB A,X,IBDR ;se copia el dato al registros de datos para ser transmitido
    INC INDEX_RTC
FIN_WRITE_RTC:
    RTS

PRIMERA_WR:
    MOVB DIR_SEG,IBDR ;word address es lo primero que se debe transmitir
    CLR INDEX_RTC ;se pone en cero el indice para comenzar a transmitir los datos en orden
    BRA FIN_WRITE_RTC

STOP_WR:
    BCLR IBCR %00100000 ;MS/SL <-- 0 (Senal de stop)
    INC INDEX_RTC ;esto lo pone en 8.
    BRA FIN_WRITE_RTC
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina READ_RTC: se encarga de hacer la lectura del modulo RTC por medio
;     de la interfaz IIC.
;------------------------------------------------------------------------------
READ_RTC:
    LDAA INDEX_RTC
    CMPA #8 ;primera interrupcion
    BEQ PRIMERA_RD
    CMPA #9 ;segunda interrupcion
    BEQ REPEAT_START
    CMPA #10 ;tercera interrupcion
    BEQ DUMMY
    CMPA #6 ;penultima interrupcion
    BEQ NOAK
    CMPA #7 ;ultima interrupcion
    BEQ STOP_RD
LEER_DATO:
    LDX #T_READ_RTC ;direccion base del arreglo para recibir datos
    MOVB IBDR,A,X ;copia el dato del registro de datos del IIC
    INC INDEX_RTC
FIN_READ_RTC:
    RTS

PRIMERA_RD:
    MOVB DIR_SEG,IBDR ;word address es lo primero que se debe transmitir
    INC INDEX_RTC
    BRA FIN_READ_RTC

REPEAT_START:
    BSET IBCR %00000100 ;RSTA <-- 1 para repeated start
    MOVB DIR_RD,IBDR ;se carga el slave address para lectura
    INC INDEX_RTC
    BRA FIN_READ_RTC

DUMMY:
    BCLR IBCR %00010100 ;RSTA <-- 0 apaga repeated start, Tx/Rx <-- 0 pone master en modo receptor
    CLR INDEX_RTC ;se reinicia indice para leer en orden los datos.
    LDAA IBDR ;lectura dummy para enviar los ciclos de reloj
    BRA FIN_READ_RTC

NOAK:
    BSET IBCR %00001000 ;TXAK <-- 1 para que no se envie el aknowledge y pare la transmision
    BRA LEER_DATO

STOP_RD:
    BCLR IBCR %00101000 ;TXAK <-- 0 para borrar el NOAK, MS/SL <-- 0 (senal de stop)
    BSET IBCR %00010000 ;TX/RX <-- 1 para poner modo transmision
    INC INDEX_RTC
    BRA FIN_READ_RTC
;------------------------------------------------------------------------------
