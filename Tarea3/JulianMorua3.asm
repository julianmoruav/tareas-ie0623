;##############################################################################
;                                 Tarea #3
;   Fecha: 8 de octubre del 2019.
;   Autor: Julian Morua Vindas
;
;   Descripcion: Este programa implementa 4 subrutinas en donde se utilizan las
;       tres subrutinas del DeBug12: getchar, putchar y printf. Primero se da la
;       lectura de un numero decimal de dos posiciones entre 01 y 99 en la
;       subrutina LEER_CANT. Este numero indica cuantos datos se deben buscar en
;       una tabla definida. Los datos encontrar son tales que deben tener una
;       raiz cuadrada entera. En la subrutina BUSCAR es donde se da esta busqueda
;       y cuando se encuentra el dato se le calcula la raiz cuadrada por medio
;       del algoritmo babilonico implementado en la subrutina RAIZ. Una vez
;       calculada la raiz cuadrada entera se almacenan todos los resultados en
;       un arreglo. Finalmente, la subrutina PRINT_RESULT despliega los resultados.
;##############################################################################


;------------------------------------------------------------------------------
;                            Definicion de simbolos
;------------------------------------------------------------------------------
CR:         EQU $0D  ;caracter ASCII para restaurar el cursor.
LF:         EQU $0A  ;caracter ASCII para cambiar el cursor a una linea nueva
FIN:        EQU $00  ;caracter ASCII para NULL. Necesario para terminar las hileras de caracteres.
getchar:    EQU $EE84 ;direccion de la subrutina del debugger getchar
putchar:    EQU $EE86 ;direccion de la subrutina del debugger getchar
printf:     EQU $EE88 ;direccion de la subrutina del debugger getchar
N:          EQU 27    ;tamano del arreglo DATOS y ENTERO
;------------------------------------------------------------------------------
;                        Declaracion de las estructuras de datos
;------------------------------------------------------------------------------
            ORG $1000 ;los datos se guardan en RAM
LONG:       DB N ;constante tipo byte para la cantidad de datos
CANT:       DS 1 ;variable tipo byte. Es definida por el usuario para indicar cuantos datos desea buscar.
CONT:       DS 1 ;variable tipo byte con el tamano del arreglo ENTERO.
CUAD_LONG:  DB 15 ;constante tipo byte para la cantidad de cuadrados perfectos posibles en un byte.

            ORG $1020
DATOS:      DB 81,13,15,100,25,144,8,9,1,0,12,121,196,36,0,22,225,64,49,2,4,4,4,16,169,196,42 ;tabla de datos bytes.

            ORG $1040
CUAD:       DB 1,4,9,16,25,36,49,64,81,100,121,144,169,196,225 ;tabla con todos los cuadrados perfectos posibles en un byte.

            ORG $1100
ENTERO:     DS N ;arreglo con las raices cuadradas enteras encontradas. Es de tamno CONT.

            ORG $1300 ;variables para las subrutinas:
POS:        DS 1 ;variable tipo byte utilizada en LEER_CANT que representa cuantas posiciones tiene el numero decimal.

XBAB:       DS 2 ;variable tipo word que representa la variable x en el algoritmo babilonico
RBAB:       DS 2 ;variable tipo word que representa la variable r en el algoritmo babilonico
TBAB:       DS 2 ;variable tipo word que representa la variable t en el algoritmo babilonico

DATOS_FIN:  DS 2 ;variable tipo word con la direccion de fin de la tabla DATOS. Se utiliza para la condicion de parada.
CUAD_FIN:   DS 2 ;variable tipo word con la direccion de fin de la tabla CUAD.  Se utiliza para la condicion de parada.

;------------------------------------------------------------------------------
;                       Definicion de los mensajes
;------------------------------------------------------------------------------
            ORG $1400
INGRESAR:   DB CR,LF,CR,LF
            FCC "--------------------------------------------"
            DB CR,LF
            FCC "Ingrese el valor de CANT (entre 1 y 99): "
            DB FIN ;Mensaje inicial de subrutina LEER_CANT.

ERR_INGR:   DB CR,LF,CR,LF
            FCC "El valor 00 no es aceptado. Intente de nuevo."
            DB CR,LF,FIN ;Mensaje de error para subrutina LEER_CANT.

CONT_PRINT: DB CR,LF,LF
            FCC "Cantidad de numeros encontrados: %u"
            DB CR,LF,FIN

ENTERO_PRINT: DB CR,LF,LF
            FCC "ENTERO: %u"
            DB FIN

NUM_PRINT:  FCC ", %u"
            DB FIN

;------------------------------------------------------------------------------
;                             Programa principal
;------------------------------------------------------------------------------
            ORG $2000 ;programa en RAM
MAIN:       LDS #$3BFF ;se inicializa puntero de pila en direcciones inferior a las del Debugger.

            JSR LEER_CANT
            JSR BUSCAR
            JSR PRINT_RESULT
            JMP MAIN ; bucle cerrado para el programa principal.

;------------------------------------------------------------------------------

;*******************************************************************************
;Subrutina LEER_CANT: permite que el usuario ingrese un numero decimal de dos
;      posiciones entre 01 y 99. No recibe parametros y su valor de retorno se
;      almacena en la variable CANT.
;------------------------------------------------------------------------------
LEER_CANT:  LDAA #2
            STAA POS        ;un numero decimal entre 1 y 99 tiene dos posiciones.
            CLR CANT        ;inicialmente CANT es 0.
            LDX #0
            LDD #INGRESAR
            JSR [printf,X]  ;se imprime el mensaje inicial.

LEER_LOOP:  LDX #0
            JSR [getchar,X] ;se da la lectura de un caracter del teclado.
            PSHD            ;se apila el caracter para su uso posterior.
            SUBD #$30       ;se obtiene el valor numero caracter ASCII
            CMPB #9
            BLS LEER_VALID  ;El salto se toma solo si el resultado de la resta esta entre 0 y 9. Note que cuando se presiona un caracter con valor ASCII menor a $30, la resta daria un numero que si se interpreta sin signo seria mayor que 9.
            LEAS 2,SP       ;el caracter leido no es valido y por lo tanto se desapila.
            BRA LEER_LOOP   ;se debe continuar la lectura hasta se ingrese un caracter correcto

LEER_VALID: ADDB CANT       ;se suma el valor del caracter leido al valor acumulado de CANT.
            DEC POS
            BEQ LEER_FIN    ;cuando POS es cero significa que el valor que se acaba de sumar es el ultimo digito (las unidades).
            LDAA #10        ;multiplicador de 10 para pasar el contenido de CANT a la siguiente posicion decimal (las decenas).
            MUL
            STAB CANT
            PULD            ;se desapila el caracter ASCII leido
            LDX #0
            JSR [putchar,X] ;se despliega en pantalla el caracter ASCII leido, a manera de mostrar que fue aceptado.
            BRA LEER_LOOP   ;se vuelve a leer otro caracter

LEER_FIN:   STAB CANT       ;se almacena el valor final de CANT.
            CMPB #0
            BEQ LEER_ERROR  ;si CANT es 00 se debe mosrar el mensaje de error.
            PULD            ;se desapila el caracter ASCII leido
            LDX #0
            JSR [putchar,X] ;se despliega en pantalla el caracter ASCII leido, a manera de mostrar que fue aceptado.
            LDD #CR
            LDX #0
            JSR [putchar,X] ;se lleva el cursor al origen.
            LDD #LF
            LDX #0
            JSR [putchar,X] ;se cambia el cursor de linea.
            RTS             ;termina la subrutina

LEER_ERROR: LDX #0
            PULD
            JSR [putchar,X]
            LDX #0
            LDD #ERR_INGR
            JSR [printf,X]  ;se imprime el mensaje de error.
            BRA LEER_CANT   ;se vuelve a ejecutar toda la subrutina de nuevo.
;------------------------------------------------------------------------------


;*******************************************************************************
;Subrutina RAIZ: calcula un aproximacion de la raiz cuadrada de un numero ENTERO
;     con el algoritmo Babilonico. El valor al que se le quiere calcular la raiz
;     se pasa como un parametro por pila; el retorno tambien se da por pila.
;------------------------------------------------------------------------------
RAIZ:       PULY            ;se protege el contador de programa.
            PULD            ;se desapila el parametro.
            STAD XBAB       ;el parametro es la variable XBAB
            STAD RBAB       ;condiciones iniciales del algoritmo
            CLR TBAB        ;condiciones inciales del algoritmo

BAB_ITERA:  MOVW RBAB,TBAB  ;a partir de esta etiqueta se implementa el algoritmo
            LDAD XBAB       ;se carga en la parte baja de D la variable XBAB. D es el dividendo.
            LDX RBAB       ;RBAB es el divisor, que se carga en X.
            IDIV            ;XBAB/RBAB
            TFR X,D         ;El cociente queda en el registro X, pero se requiere tenerlo en D.
            ADDD RBAB       ;+RBAB
            LSRD            ;/2.
            STAD RBAB
            CMPD TBAB
            BNE BAB_ITERA   ;si se produce el mismo resultado que la iteracion anterior el algoritmo termina.
            PSHD            ;se retorna el resultado por pila
            PSHY            ;se restaura el contador de programa.
            RTS             ;termina la subrutina
;------------------------------------------------------------------------------


;*******************************************************************************
;Subrutina BUSCAR: Busca los CANT datos de DATOS que estan contenidos en CUAD,
;     calcular su raiz cuadrada y los almacena en ENTERO. Ademas, en la variable
;     CONT se llueva la cuenta de cuantos datos fueron encontrados.
;------------------------------------------------------------------------------
BUSCAR:     CLRA
            LDAB LONG
            ADDD #DATOS
            STAD DATOS_FIN  ;se calcula y almacena la direccion final de DATOS
            CLRA
            LDAB CUAD_LONG
            ADDD #CUAD
            STAD CUAD_FIN   ;se calcula y almacena la direccion final de CUAD

            LDX #DATOS      ;se inicia el indice para tabla DATOS.
            LDY #ENTERO    ;se inicia el indice para arreglo ENTERO
            PSHY            ;se apila el indice para ENTERO, se requiere solo cuando se va a escribir.
            CLR CONT        ;se inicia la variable CONT en 0.

LOOP_DATOS: LDY #CUAD       ;se lleva al inicio el indice para tabla CUAD.
            LDAA 1,X+       ;se carga el dato de la tabla DATOS y se deja X apuntando al siguiente.
LOOP_CUAD:  LDAB 1,Y+       ;se carga el cuadrado perfecto y se deja Y apuntando al siguiente.
            CBA             ;se compara el dato con el cuadrado perfecto
            BEQ CALCULAR    ;si son iguales entonces se calcula la raiz cuadrada.
STOP_CUAD:  CPY CUAD_FIN    ;se comprueba si ya se recorrio toda la tabla CUAD
            BNE LOOP_CUAD   ;si no se ha llegado al final de CUAD entonces quedan posibles cuadrados perfectos por comparar.
STOP_DATOS: CPX DATOS_FIN   ;se comprueba si ya se recorrio toda la tabla DATOS
            BNE LOOP_DATOS  ;si no se ha llegado al final de DATOS entonces quedan datos por analizar. Siempre que CONT no sea 0.

RETORNO:    LEAS 2,SP       ;se desapila el indice para ENTERO que fue apilado al principio de la subrutina.
            RTS             ;fin de la subrutina

CALCULAR:   PSHX            ;se apila el indice de la tabla DATOS, para seguirla recorriendo despues.

            CLRA            ;se limpia la parte alta del registro D porque el parametro de entrada a RAIZ es un byte.
            PSHD            ;se pasa por la pila el parametro de la subrutina RAIZ.
            JSR RAIZ
            PULD            ;se desapila el resultado de la raiz cuadrada.
            PULX            ;se desapila el indice de la tabla DATOS.

            PULY            ;se desapila el indice del arreglo ENTERO para realizar la escritura del resultado.
            STAB 1,Y+       ;se escribe el resultado y se deja el indice apuntado a la siguiente posicion de escritura
            PSHY            ;se apila el indice de escritura en el arreglo ENTERO.
            INC CONT        ;se aumenta el contador de escrituras.

            DEC CANT        ;se disminuye la cantidad de datos por buscar.
            BEQ RETORNO     ;se encontraron la cantidad de datos solicitados y la subrutina puede terminar.
            BRA STOP_DATOS  ;se debe comprobar si todavia quedan datos por analizar.
;------------------------------------------------------------------------------

;*******************************************************************************
;Subrutina PRINT_RESULT:  recorre el arreglo ENTERO e imprimer su contenido.
;------------------------------------------------------------------------------
PRINT_RESULT: CLRA          ;se limpia parte alta de D
            LDAB CONT       ;se carga CONT en parte baja de D
            PSHD            ;se apila CONT en 16 bits
            LDD #CONT_PRINT
            LDX #0
            JSR [printf,X]  ;se imprime el CONT
            PULX            ;se desapila CONT en registro indice
            CMPX #0
            BEQ FIN_PRINT   ;se comprueba que se hayan encontrado raices cuadradas enteras
            LDD #ENTERO     ;se carga direccion de inicio del arreglo ENTERO
            LEAX D,X        ;se reposiciona el indice en el final del arreglo

APILAR_ENT: CLRA            ;se limpia la parte alta de D
            LDAB 1,-X       ;se apunta el indice al dato anterior y luego se lee el dato.
            CPX #ENTERO
            BLO PRINT_ENT   ;se comprueba si ya se recorrio todo el arreglo (de fin a inicio)
            PSHD            ;se apila el dato para imprimirlo.
            BRA APILAR_ENT  ;Note que se esta apilando de fin inicio porque printf requiere los datos a imprimir acomodados en la pila en orden inverso.

PRINT_ENT:  LDX #0
            LDD #ENTERO_PRINT ;se prepara el nombre del arreglo y el primer dato para ser impresos
PRINT_LOOP: JSR [printf,X]  ;se imprime el dato
            LEAS 2,SP       ;se reposiciona el puntero de pila para descontar el dato recien impreso.
            LDX #0
            LDD #NUM_PRINT  ;se prepara el siguiente dato para ser impreso
            DEC CONT        ;se reduce la cuenta de datos impresos
            BNE PRINT_LOOP  ;al llegar a cero no hay mas datos por imprimir y el programa termina.
FIN_PRINT:  RTS             ;termina la subrutina
;------------------------------------------------------------------------------
