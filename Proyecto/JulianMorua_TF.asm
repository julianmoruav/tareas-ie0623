;##############################################################################
;                        Trabajo Final: Radar 623
;   Fecha: 9 de diciembre del 2019.
;   Autor: Julian Morua Vindas
;
;   Descripcion:
;       El RADAR 623 es un dispositivo configurable para la medición de la velocidad
;       de un automóvil en carretera, que se implementa en la tarjeta Dragon 12+.
;       Dispone de tres modos: el de configuración, el de medición y el libre.
;       En el modo libre, el RADAR 623 apaga sus recursos de medición y se encuentra
;       en un estado ocioso.
;
;       La velocidad límite para transitar es ingresada a través del modo de
;       configuración; en este, el usuario debe utilizar un teclado matricial
;       provisto por el RADAR 623 para guardar un número entre 45 km/h y 90 km/h,
;       inclusivos. Si se ingresa un valor por fuera de este rango, el RADAR 623
;       rechaza la acción y despliega un cero que indica que la velocidad límite
;       no se ha configurado correctamente. No es posible pasar del modo de configuración
;       al modo de medición si no se ha guardado una velocidad límite válida.
;
;       En el modo de medición, el RADAR 623 dispone de dos sensores
;       (simulados por los botones PH3 y PH0) que están separados entre sí una
;       distancia conocida de 40 m. El paso de un vehículo por el primer sensor
;       (S1) inicia una cuenta de tiempo que es concluida cuando el vehículo supera
;       el segundo sensor (S2). Con este tiempo y la distancia entre sensores,
;       el RADAR 623 calcula la velocidad promedio a la que se transita. Si la
;       velocidad de tránsito calculada supera la velocidad límite configurada,
;       entonces el RADAR623 enciende una alerta en la que los LEDS PB7 a PB3 son
;       barridos. Además, a 200 m del sensor S2 se encuentra una pantalla LCD y
;       cuatro displays de siete segmentos que se utilizan para desplegar información
;       relevante. Con la velocidad de tránsito que se calculó, el RADAR 623 estima
;       cuando el vehículo se encuentra a 100 m de la pantallas, y en este momento
;       muestra la velocidad de tránsito y la velocidad límite de la carretera;
;       esta información se mantiene desplegada hasta que el vehículo supera las
;       pantallas y luego de este momento el RADAR 623 queda disponible para la
;       medición de un nuevo vehículo.
;
;       En todo momento la pantalla LCD muestra información sobre el estado del
;       RADAR 623 y sobre qué es la información que se muestra en los displays de
;'      siete segmentos. Además, se tienen tres luces LED (PB0 a PB2) que indican
;       cuál es el modo en el que se encuentra el RADAR 623. La selección del modo
;       se hace cambiando los switches PH7 y PH6 de posición. Por último, el RADAR 623
;       ofrece un ajuste de brillo para los displays de siete segmentos y las
;       luces LED, esto se logra por medio de la lectura de la tensión a través
;       de un potenciómetro.
;##############################################################################
#include registers.inc

;------------------------------------------------------------------------------
;     Declaracion de las estructuras de datos y vectores de interrupcion
;------------------------------------------------------------------------------
;Vectores de interrupcion:
               ORG $3E70   ;direccion del vector de interrupcion RTI.
               DW RTI_ISR  ;direccion de la subrutina de servicio a interrupcion RTI.
               ORG $3E4C   ;direccion del vector de interrupcion por key wakeup del puerto H.
               DW CALCULAR  ;direccion de la subrutina de servicio a interrupcion del puerto H.
               ORG $3E66   ;direccion del vector de interrupcion OC4.
               DW OC4_ISR  ;direccion de la subrutina de servicio a interrupcion OC4.
               ORG $3E52   ;direccion del vector de interrupcion ATD0.
               DW ATD_ISR ;direccion de la subrutina de servicio a interrupcion ATD0.
               ORG $3E5E   ;direccion del vector de interrupcion TCNT Timer Overflow
               DW TCNT_ISR ;direccion de la subrutina de servicio a interrupcion TCNT Timer Overflow


;Estructuras de datos:
                ORG $1000
BANDERAS:       DS 2  ;Es un word donde cada bit representa una bandera que se explica a continuacion:
                      ;0: TCL_LISTA indica que luego de la supresion de rebotes se confirmo que si se presiono una tecla.
                      ;1: TCL_LEIDA indica que ya se habia tenido una lectura del teclado y que se estaba esperando a que se diera la supresion de rebotes.
                      ;2: ARRAY_OK indica que se presiono la tecla Enter y que en el arreglo ya se tienen todos los valores leidos.
                      ;3: PANT_FLAG indica cuando se debe desplegar informacion en pantalla
                      ;4: ALERTA indica que esta por encima del limite y se debe realizar el barrido de LEDS.
                      ;5: CALC_TICKS indica si ya se dio el calculo de TICK_EN y TICK_DIS
                      ;6: MODO0 el bit menos significativo en el codigo de modo. Asignado por PH6
                      ;7: MODO1 el bit mas significativo en el codigo de modo. Asignado por PH7
                      ;8: CAMBIO_MODO indica que acaba de ocurrir un cambio y se deben actualizar pantallas y variables.
                      ;9: OUT_RANGE indica si VELOC esta fuera de rango

V_LIM:          DS 1  ;es la velocidad limite ingresada por el usuario en el modo config. Esta entre 45 y 90 km/h, inclusivos.
MAX_TCL:        DB 2  ;cantidad maximas de teclas que se leen
Tecla:          DS 1  ;en esta variable se almacena el valor leido del teclado en la subrutina MUX_TECLADO.
Tecla_IN:       DS 1  ;en esta variable se almacena temporalmente el valor de Tecla antes de la supresion de rebotes. Si despues de la supresion se da que Tecla y Tecla_IN son iguales es porque efectivamente se presiono una tecla que debe ser guardada.
Cont_Reb:       DS 1  ;es el contador de ticks del RTI, usado para suprimir rebotes.
Cont_TCL:       DS 1  ;es el indice utilizado para escribir en el arreglo que guarda las teclas presionadas.
Patron:         DS 1  ;es el indice que lleva las iteraciones en subrutina MUX_TECLADO.
Num_Array:      DS 2  ;en este arreglo se almacenan todas las teclas presionadas por el usuario.

BRILLO:         DS 1  ;brillo a partir de la medicion del potenciometro.
POT:            DS 1  ;Variable controlada por potencionmetro para incrementar/decrementar el brillo.

TICK_EN:        DS 2  ;para contar el tiempo que toma en estar a 100m de la pantalla
TICK_DIS:       DS 2  ;para contar el tiempo que toma en estar a 200m de la pantalla

VELOC:          DS 1  ;la velocidad medida

TICK_VEL:       DS 1  ;para medir el tiempo entre sensores y poder calcular la velocidad.



BIN1:           DS 1  ;variable de entrada a subrutina CONV_BIN_BCD.
BIN2:           DS 1  ;variable de entrada a subrutina CONV_BIN_BCD.
BCD1:           DS 1  ;variable de salida de subrutina CONV_BIN_BCD. Entrada para BCD_7SEG.
BCD2:           DS 1  ;variable de salida de subrutina CONV_BIN_BCD. Tambien es entrada para BCD_7SEG.

LOW:            DS 1  ;variable requerida para el algoritmo de la subrutina BIN_BCD
BCD_L:          DS 1
DISP1:          DS 1  ;corresponde al valor que se escribe en el display de 7 segmentos.
DISP2:          DS 1  ;BCD2 utiliza DISP1 y DISP2 para desplegarse
DISP3:          DS 1  ;corresponde al valor que se escribe en el display de 7 segmentos.
DISP4:          DS 1  ;BCD1 utiliza DISP3 y DISP4 para desplegarse

LEDS:           DS 1  ;guarda el estado de los LEDS. LED PB1 corresponde a modo CONFIG, LED PB0 a modo RUN.
CONT_DIG:       DS 1  ;cuenta cual digito de 7 segmentos se debe habilitar. Cambia cada vez que CONT_TICKS alcanza 100.
CONT_TICKS:     DS 1  ;contador de ticks de Output Compare para multiplexar.
DT:             DS 1  ;ciclo de trabajo. DT = N-K.
CONT_7SEG:      DS 2  ;contador de ticks de OC4 para lograr refrescamiento de LEDS y Displays a 10 Hz.
CONT_200:       DS 2  ;contador de ticks de OC4 para lograr barrido de LEDS y conversion ATD a 5 Hz.

Cont_Delay:     DS 1  ;esta variable se carga con alguna de las siguientes tres constantes para generar retrasos temporales.
D2ms:           DB 100  ;100 ticks a 50kHz son 2 milisegundos
D260us:         DB 13   ;13 ticks a 50kHz son 260 microsegundos
D40us:          DB 2    ;2 ticks a 50kHz son 40 microsegundos
CLEAR_LCD:      DB $01  ;comando para limpiar el LCD
ADD_L1:         DB $80  ;direccion inicio de linea 1
ADD_L2:         DB $C0  ;direccion inicio de linea 2
FUNCTION_SET:   EQU $28
ENTRY_MODE_SET: EQU $06
DISPLAY_ON:     EQU $0C
EOM:            EQU $00

                ORG $1030
Teclas:         DB $01,$02,$03,$04,$05,$06,$07,$08,$09,$0B,$00,$0E ;valores de las teclas

                ORG $1040
SEGMENT:        DB $3F,$06,$5B,$4F,$66,$6D,$7D,$07,$7F,$6F,$40,$00 ;patrones para el display de 7 segmentos.

                ORG $1050
iniDsp:         DB 4,FUNCTION_SET,FUNCTION_SET,ENTRY_MODE_SET,DISPLAY_ON

                ORG $1060 ;mensajes
CONFIG_MSJ1:    FCC "  MODO CONFIG."
                DB EOM
CONFIG_MSJ2:    FCC " VELOC. LIMITE"
                DB EOM
LIBRE_MSJ1:       FCC "  RADAR 623"
                DB EOM
LIBRE_MSJ2:       FCC "  MODO LIBRE"
                DB EOM
MEDI_MSJ1:      FCC " MODO MEDICION"
                DB EOM
MEDI_MSJ2:      FCC "SU VEL. VEL.LIM"
                DB EOM
CALC_MSJ:       FCC "  CALCULANDO..."
                DB EOM
ESPERA_MSJ:     FCC "  ESPERANDO... "
                DB EOM


;------------------------------------------------------------------------------



;*******************************************************************************
;                             Programa principal
;*******************************************************************************
    ORG $2000
;--------------------------
;Configuracion del hardware
;--------------------------
;Inicializa la pila:
    LDS #$3BFF

;Habilita interrupciones mascarables:
    CLI

;Configuracion del ATD0:
    MOVB #$C2,ATD0CTL2 ;habilita convertidor ATD0, borrado rapido de banderas y las interrupciones.
    LDAA #160
CONFIG_ATD:
    DBNE A,CONFIG_ATD ;3ciclosCLK * 160cuentas * (1/48 MHz) = 10 us. Tiempo requerido para que inicie el ATD.
    MOVB #$30,ATD0CTL3 ;ciclo de 6 conversiones
    MOVB #$B7,ATD0CTL4 ;conversion a 8 bits, tiempo final de muestreo es 4 periodos, f_sample = 500 kHz (PRS=23)
    MOVB #$87,ATD0CTL5 ;justifica a la derecha. Sin signo. No multiplexacion. Canal 7 es el del pot. Escritura inicia ciclo.

;Configuracion RTI:
    BSET CRGINT %10000000 ;se habilita RTI
    MOVB #$31,RTICTL      ;periodo de 1.024 ms

;Configuracion keywakeup en puerto H:
    BCLR PPSH %00001001   ;las interrupciones deben ocurrir en el flanco decreciente.

;Configuracion PH7 y Ph6 como entrada de proposito general por polling: (Dipswitches)
    BCLR DDRH %11000000

;Configuracion del teclado en puerto A:
    MOVB #$F0,DDRA        ;parte alta de A como salida y parte baja como entrada
    BSET PUCR %00000001   ;resistencias de pull-up en puerto A. Son necesarias para que haya un 1 en el PAD cuando no se presiona ningun boton del teclado.

;Configuracion del modulo de Timer (OC4 y TCNT):
    BSET TSCR1 %10000000 ;se habilita modulo de timer.
    BSET TSCR2 %00000011 ;prescaler es 2^3 = 8. El bit 7 es TOI que debe habilitarse en modo medicion.
    BSET TIOS %00010000 ;se configura el canal 4 como Output Compare.
    BSET TIE %00010000 ;se habilita interrupcion del canal 4.
    BCLR TCTL1 $00000011 ;no es necesario que haya una salida en puerto T. Solo se requiere la interrupcion.

;Configuracion de los displays de 7 segmentos y los LEDS.
    MOVB #$FF,DDRB ;puerto core B se configura como salida de proposito general. (LEDS y SEGMENTOS)
    MOVB #$0F,DDRP ;parte baja de puerto P se configura como salida de proposito general. (~Habilitador Segmentos)
    BSET DDRJ %00000010 ;se configura bit 1 del puerto J como salida de proposito general . (~Habilitador LEDS)

;Configuración de pantalla LCD
    MOVB #$FF,DDRK ;todos los pines del puerto K se configura como salida para controlar la LCD.
    JSR INIT_LCD

;---------------------------
;Inicializacion de variables
;---------------------------
;Teclado matricial:
    MOVB #$FF,Tecla
    MOVB #$FF,Tecla_IN
    MOVB #$FF,Num_Array
    CLR Cont_Reb
    CLR Cont_TCL
    CLR Patron

;Displays de 7 segmentos y LCD:
    MOVW #1,CONT_7SEG ;se carga con 1 porque se descuenta al entrar a OC4_ISR
    MOVW #1,CONT_200 ;se carga con 1 porque se descuenta al entrar a OC4_ISR
    CLR CONT_TICKS
    CLR CONT_DIG
    CLR BCD1
    CLR BCD2

;Programa:
    CLR BANDERAS ;OUT_RANGE = 0, CAMBIO_MODO= = 0
    CLR BANDERAS+1 ;MODO1 = 0, MODO0 = 0, CALC_TICKS = 0, ALERTA = 0, PANT_FLAG, ARRAY_OK = 0, TCL_LEIDA = 0, TCL_LISTA = 0
    BSET BANDERAS %00000001 ; CAMBIO_MODO = 1, inicialemente queremos imprimir el modo inicial
    CLR V_LIM
    CLR VELOC
    CLR TICK_VEL
    CLR TICK_EN
    CLR TICK_DIS

;---------------------------
;Programa princial:
;---------------------------

MAIN_LOOP:
    JSR LEER_MODO
    BRSET BANDERAS+1 %01000000 TO_MEDI ;si MODO0=1 entonces es medicion
    BRSET BANDERAS+1 %10000000 TO_LIBRE ;si MODO0=0 y MODO1=1 entonces es libre
    JSR MODO_CONFIG ;si MODO0=0 y MODO1=0 entonces config
    BRA MAIN_LOOP
TO_MEDI:
    JSR MODO_MEDICION
    BRA MAIN_LOOP
TO_LIBRE:
    JSR MODO_LIBRE
    BRA MAIN_LOOP
;*******************************************************************************

;------------------------------------------------------------------------------
;   Subrutina de servicio a interrupcion RTI: Esta subrutina descuenta Cont_Reb
;     siempre y cuando no sean cero. Los ticks del RTI duran 1.024 ms, por lo
;     que si se cargan variables con X valor se pueden contar aproximadamente
;     X milisegundos. Se utiliza para suprimir rebotes contando ~20ms. Cont_Reb
;     es el unico parametro de entrada y salida que se pasa por memoria
;------------------------------------------------------------------------------
RTI_ISR:
    TST Cont_Reb
    BEQ FIN_RTI_ISR       ;si el contador ya esta en cero no hay nada que hacer.
    DEC Cont_Reb
FIN_RTI_ISR:
    BSET CRGFLG %10000000  ;reinicia la bandera de interrupcion
    RTI
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina de servicio a interrupcion keywakeup en puerto H: las fuentes de
;     interrupcion son PH3 y PH0, sin embargo, solo una esta activa a la vez.
;     Se espera primero la interrupcion PH3 correspondiente al sensor S1 y cuando
;     esta llega se deshabilita PH3, se habilita PH0 y se reinicia TICK_VEL.
;     Cuando el vehiculo llega a S2 se da la interrupcion PH3 y se procede a
;     deshabilitar PH3 y calcular la velocidad a partir de TICK_VEL. Por
;     direccionamiento directo a memoria: TICK_VEL (I/O), Cont_Reb (I/0), VELOC (O)
;
;     Se parte de que T_TOI = 0.0218s, d = 40m y 3.6 km/h = 1 m/s:
;         VELOC = 3.6•(d÷t)  [km/h]
;         VELOC = 3.6•(d÷(TICK_VEL•T_TOI))
;     Pero T_TOI es fracionario, entonces se escala su valor:
;         T_TOI•10000 = 218
;     --> VELOC = 3.6•(40•10000)÷(TICK_VEL•218)
;     Finalemente:
;         VELOC = (36•40•1000)÷(TICK_VEL•218)
;------------------------------------------------------------------------------
CALCULAR:
    TST Cont_Reb
    BNE FIN_CALCULAR

SENSOR_S1:
    BRCLR PIFH %00001000 SENSOR_S2
    BCLR PIEH %00001000 ;se deshabilita interrupcion por PH3 (Sensor S1)
    BSET PIEH %00000001 ;se habilita interrupcion por PH0 (Sensor S2)
    CLR TICK_VEL ;empieza la cuenta de tiempo
    MOVB #20,Cont_Reb ;empieza el contador de rebotes
    ;esto es para imprimi CALCULANDO cuando se supera PH3
    ; LDX #MEDI_MSJ1
    ; LDY #CALC_MSJ
    ; CLI ;se habilitan interrupciones mascarables porque Cargar_LCD las requiere
    ; JSR Cargar_LCD
    ; SEI ;se deshabilitan interrupciones mascarables.
    ;hasta aqui
    BRA FIN_CALCULAR

SENSOR_S2:
    ;BRCLR PIFH %00000001 FIN_CALCULAR
    BCLR PIEH %00000001 ;se deshabilita interrupcion por PH0 (Sensor S2)
    LDAA #218 ;T_TOI escalado por 10000
    LDAB TICK_VEL ;ticks de TOI
    MUL ;D <-- A*B = T_TOI*10000*TICKS_VEL = tiempo entre sensores escalado por 10000
    PSHD ;se apila el denominador
    LDAA #40 ;distancia entre sensores en metros
    LDAB #36 ;factor de conversion de m/s a km/h, escalado por 10.
    MUL ;D <-- A*B = d*conversion = 40*360
    LDY #1000 ;factor para cancelar el escalamiento de T_TOI (10000).
    EMUL ; Y:D <-- D*Y =  40*360*1000 = 40*3,6*10000
    PULX ;se carga el denomidor
    EDIV ;Y <-- (Y:D)/X = 40*3,6*10000/(T_TOI*10000*TICK_VEL) = 40*3,6/(T_TOI*TICK_VEL) = Velocidad promedio en km/h
    TFR Y,D
    CPD #$FF
    BHI MUY_RAPIDO
GUARDAR_VELOC:
    STAB VELOC
    MOVB #20,Cont_Reb ;empieza el contador de rebotes
    ;esto es para imprimir CALCULANDO cuando se supera PH0:
    LDX #MEDI_MSJ1
    LDY #CALC_MSJ
    CLI ;se habilitan interrupciones mascarables porque Cargar_LCD las requiere
    JSR Cargar_LCD
    SEI ;se deshabilitan interrupciones mascarables.
    ;hasta aqui
    CLR TICK_VEL


FIN_CALCULAR:
    BSET PIFH %00001001 ;apaga las fuentes de interrupcion
    RTI

MUY_RAPIDO:
    LDAB #$FF
    BRA GUARDAR_VELOC
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina ATD_ISR: luego de que el ATD realiza las 6 mediciones al canal 7
;     esta subruina calcula el promedio de esas mediciones y lo guarda en la
;     variable POT. Además calcula la variable BRILLO. Parametros por
;     direccionamiento directo a memoria: Registros de datos ADRXX (I), POT (O), BRILLO (O).
;------------------------------------------------------------------------------
ATD_ISR:
    LDD ADR00H
    ADDD ADR01H
    ADDD ADR02H
    ADDD ADR03H
    ADDD ADR04H
    ADDD ADR05H ;En D se tiene la sumatoria de las mediciones al canal 7 del ATD.
    LDX #6 ;carga el divisor de 6 para calcular el promedio
    IDIV ;X <-- D/X = sumatoria/6 = promdio
    TFR X,D
    STAB POT
    LDAA #20
    MUL ;D <-- A*B = 20*POT
    LDX #255
    IDIV ;X <-- D/X = 20*POT/255 = BRILLO
    TFR X,D
    STAB BRILLO
    ;MOVB #$87,ATD0CTL5 ;al escribir a ATD0CTL5 se inicia un nuevo ciclo de conversion
    RTI
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina de servicio a interrupcion por output compare en el canal 4:
;     Descuenta Cont_Delay, inicia el ciclo de conversion ATD, refresca cada 100 ms
;     (5000ticks) los valores de DISP1-DISP4, multiplexa el bus del puerto B para
;     mostrar informacion en los displays de 7 segmentos y los LEDS, y todo con
;     un ciclo de trabajo variable que depende de BRILLO. OC4 interrumpe a 50 kHz (60 ticks).
;     Por direccionamiento directo a memoria: CONT_200 (I/O), CONT_7SEG (I/O),
;     Cont_Delay (I/O),
;------------------------------------------------------------------------------
OC4_ISR:
    LDD CONT_200 ;por tratarse de un WORD se debe traer al registro D para restarle 1
    SUBD #1
    STD CONT_200 ;se guarda el nuevo valor, y esto a la vez afecta la bandera Z.
    BNE REFRESH ;cuando CONT_200=0 se inicia conversion ATD y se rotan los LEDS.
    MOVW #10000,CONT_200
    MOVB #$87,ATD0CTL5 ;al escribir a ATD0CTL5 se inicia un nuevo ciclo de conversion
    JSR PATRON_LEDS

REFRESH:
    LDD CONT_7SEG ;por tratarse de un WORD se debe traer al registro D para restarle 1
    SUBD #1
    STD CONT_7SEG ;se guarda el nuevo valor, y esto a la vez afecta la bandera Z.
    BNE DEC_DELAY ;cuando CONT_7SEG=0 se refrescan los valores de los displays.
    MOVW #5000,CONT_7SEG ;se reinicia el contador de refrescamiento de la informacion
    JSR CONV_BIN_BCD ;se calculan los nuevos valores a desplegar.
    JSR BCD_7SEG ;se refresca la informacion

DEC_DELAY:
    TST Cont_Delay
    BEQ SELECT_DISP ;se descuenta el contador solo si no es cero.
    DEC Cont_Delay

SELECT_DISP:
    LDAA #5
    LDAB BRILLO
    MUL ; D = A*B = 5*BRILLO = K
    LDAA #100 ;N
    SBA ; N-K
    STAA DT ;DT=N-K

    LDAA #100 ;N
    CMPA CONT_TICKS ;cuando CONT_TICKS=N=100 se debe cambiar de digito
    BNE MULTIPLEX ;si no es cero entonces no hay que cambiar de digito y se puede continuar
    CLR CONT_TICKS ;se reiniciar contador de ticks
    INC CONT_DIG ;se pasa al siguiente digito
    LDAA #5
    CMPA CONT_DIG ;cuando CONT_DIG alcance 5 se debe volver a colocar en 0 para que sea circular.
    BNE MULTIPLEX ;si no es 5 no hay que corregir nada y se puede continuar
    CLR CONT_DIG

MULTIPLEX:
    TST CONT_TICKS
    BNE DUTY_CYCLE ;cuando CONT_TICKS=0 se debe habiliar algun Display. Si no, se puede pasar a comprobar el ciclo de trabajo
    MOVB #$02,PTJ ;se deshabilitan los LEDS
    MOVB #$FF,PTP ;se deshabilitan displays de 7 segmentos
    LDAA CONT_DIG ;se comparan todos los posibles valores para determinar cual display encender.
    CMPA #0
    BEQ DIG0
    CMPA #1
    BEQ DIG1
    CMPA #2
    BEQ DIG2
    CMPA #3
    BEQ DIG3
;Ningun Display se debe habilitar, entonces son los LEDS.
    MOVB #$00,PTJ ;se habilitan los LEDS
    MOVB LEDS,PORTB ;se coloca en puerto B el estado de los LEDS.
    BRA DUTY_CYCLE ;se pasa a comprobar el ciclo de trabajo

DIG0:
    MOVB #$F7,PTP ;se habilita unicamente el display 4
    MOVB DISP4,PORTB ;se coloca en el puerto B el valor del display 4
    BRA DUTY_CYCLE

DIG1:
    MOVB #$FB,PTP ;se habilita unicamente el display 3
    MOVB DISP3,PORTB ;se coloca en el puerto B el valor del display 3
    BRA DUTY_CYCLE

DIG2:
    MOVB #$FD,PTP ;se habilita unicamente el display 2
    MOVB DISP2,PORTB ;se coloca en el puerto B el valor del display 2
    BRA DUTY_CYCLE

DIG3:
    MOVB #$FE,PTP ;se habilita unicamente el display 1
    MOVB DISP1,PORTB ;se coloca en el puerto B el valor del display 1

DUTY_CYCLE:
    LDAA CONT_TICKS
    CMPA DT
    BNE FIN_OC4_ISR
    MOVB #$FF,PTP ;se deshabilitan displays de 7 segmentos
    MOVB #$02,PTJ ;se deshabilitan los LEDS

FIN_OC4_ISR:
    INC CONT_TICKS
    BSET TFLG1 %00010000 ;se reinicia la bandera de interrupcion
    LDD TCNT ;se carga el valor actual de TCNT para reajustar el output compare
    ADDD #60 ;60 cuentas equivalen 50kHz con prescalador=8
    STD TC4 ;se actualiza el nuevo valor a alcanzar.
    RTI
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina de servicio a interrupcion TCNT_ISR: con un preescalador de 8,
;     BUSCLK a 24 MHZ, y 2^16 cuentas, esta subrutina es llamada con un periodo
;     de T_TOI = \frac{PRSx2^16}{BUSCLK} = \frac{8x2^16}{24x10^6} = 0,02184 s.
;     En esta subrutina se incrementa TICK_VEL con cada interrupcion para medir
;     el tiempo entre los sensores S1 y S2. Tambien se decrementan TICK_EN y
;     TICK_DIS para medir los tiempos en que se deben desplegar o quitar mensajes.
;     Paso de parametros por direccionamiento directo a memoria: TICK_VEL (I/O),
;     TICK_EN (I/O), TICK_DIS (I/O), PANT_FLAG (O)
;------------------------------------------------------------------------------
TCNT_ISR:
    LDAA #$FF
    CMPA TICK_VEL
    BEQ TCNT_LIM ;cuando TICK_VEL=$FF no se quiere aumentar para no rebasar.
    INC TICK_VEL ;aumento contador de ticks de velocidad
TCNT_LIM:
    LDD TICK_EN  ;esto afecta la bandera Z
    BEQ PANT_ON  ;cuando TICK_EN alcance 0 se debe poner la bandera de pantalla
    SUBD #1 ;si no es cero, se decrementa
    STD TICK_EN  ;almaceno el nuevo valor
    BRA NEXT_TCNT
PANT_ON:
    BSET BANDERAS+1 %00001000 ;PANT_FLAG <-- 1 para indicar que se debe mostrar informacion
NEXT_TCNT:
    LDD TICK_DIS  ;esto afecta la bandera Z
    BEQ PANT_OFF  ;cuando TICK_DIS alcance 0 se debe quitar la bandera de pantalla
    SUBD #1 ;si no es cero, se decrementa
    STD TICK_DIS  ;almaceno el nuevo valor
    BRA FIN_TCNT
PANT_OFF:
    BCLR BANDERAS+1 %00001000 ;PANT_FLAG <-- 1 para indicar que se debe mostrar informacion
FIN_TCNT:
    BSET TFLG2 %10000000 ;se reinicia bandera TOF para indicar que se atendio la interrupcion
    RTI
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina MODO_CONFIG: Esta subrutina corresponde a las operaciones necesarias
;     llevar a cabo la configuracion del sistema. Primero se actualizan las
;     las variables y las pantallas para que sean consistentes con el modo, esto
;     solo se hace la primera vez que se ingresa al modo. Posteriormente, con
;     el uso de TAREA_TECLADO se da la lectura del valor V_LIM. Una vez que el
;     usuario presiona ENTER se valida que el valor de V_LIM este entre 45 y 90.
;     Si es asi entonces coloca este valor en BIN1 para que pueda ser desplegado
;     en los displays 3 y 4. Cuando el valor no es valido se borra V_LIM.
;     Paso de parametros por direccionamiento directo a memoria; CAMBIO_MODO (I)
;     V_LIM (O), BIN1 (O), BIN2 (O), LEDS (O).
;------------------------------------------------------------------------------
MODO_CONFIG:
    BRCLR BANDERAS,%00000001,OK_CONFIG ;si CAMBIO_MODO=0 no hay que actualizar pantallas ni variables
    BCLR BANDERAS %00000001 ; CAMBIO_MODO<--0 para que se actualice una unica vez
    BCLR TSCR2 %10000000 ;TOI <--0 deshabilita contador de tiempo
    BCLR PIEH %00001000 ;PIEH3<--0 para apagar sensor S1
    BCLR PIEH %00000001 ;PIEH0<--0 para apagar sensor S2
    CLR VELOC
    BCLR BANDERAS+1 %00010000 ;ALERTA <--0 para apagar el barrido de LEDS.
    MOVB #$BB,BIN2 ;apaga DISP1 y DISP2
    MOVB #$01,LEDS ;PB0=ON en modo config y solo ese LED debe estar encendido
    LDX #CONFIG_MSJ1
    LDY #CONFIG_MSJ2
    JSR Cargar_LCD

OK_CONFIG:
    BRSET BANDERAS+1,%00000100,VALIDAR_V_LIM ;si ARRAY_OK=1 no se espera lectura.
    JSR TAREA_TECLADO ;lectura del teclado
    MOVB V_LIM,BIN1 ;conserva el valor de V_LIM
    RTS ;retorna. mientras ARRAY_OK=0 se seguira este ciclo que lee el teclado

VALIDAR_V_LIM:
    JSR BCD_BIN ;a partir de Num_Array se obtiene V_LIM.
    LDAA #45 ;limite inferior
    LDAB #90 ;limite superior
    CMPA V_LIM ;cuando 45 es mayor que V_LIM
    BHI INVALID_V_LIM ;se debe ignorar
    CMPB V_LIM ;cuando 90 es menor que V_LIM
    BLO INVALID_V_LIM ;se debe ignorar
FIN_CONFIG:
    BCLR BANDERAS+1,%00000100 ;ARRAY_OK=0
    MOVB V_LIM,BIN1 ;se pasa V_LIM a la entrada de CONV_BIN_BCD
    RTS

INVALID_V_LIM:
    CLR V_LIM ;se borra V_LIM cuando no es valido.
    BRA FIN_CONFIG
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina MODO_LIBRE: modo ocioso del RADAR 623 en el que se apagan recursos.
;   Paso de parametros por direccionamiento directo a memoria: CAMBIO_MODO (I)
;------------------------------------------------------------------------------
MODO_LIBRE:
    BRCLR BANDERAS,%00000001,OK_LIBRE ;si CAMBIO_MODO=0 no hay que actualizar pantallas ni variables
    BCLR BANDERAS %00000001 ; CAMBIO_MODO<--0 para que se actualice una unica vez
    BCLR TSCR2 %10000000 ;TOI <--0 deshabilita contador de tiempo
    BCLR PIEH %00001000 ;PIEH3<--0 para apagar sensor S1
    BCLR PIEH %00000001 ;PIEH0<--0 para apagar sensor S2
    CLR VELOC
    BCLR BANDERAS+1 %00010000 ;ALERTA <--0 para apagar el barrido de LEDS.
    MOVB #$BB,BIN1 ;apaga DISP3 y DISP4
    MOVB #$BB,BIN2 ;apaga DISP1 y DISP2
    MOVB #$04,LEDS ;PB2=ON en modo libre y solo ese LED debe estar encendido
    LDX #LIBRE_MSJ1
    LDY #LIBRE_MSJ2
    JSR Cargar_LCD
OK_LIBRE:
    RTS
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina MODO_MEDICION: en este modo se dan las mediciones de velocidad y se
;     despliega informacion en las pantallas a partir de PANT_CTRL.
;     Paso de parametros por direccionamiento directo a memoria: CAMBIO_MODO (I)
;------------------------------------------------------------------------------
MODO_MEDICION:
    BRCLR BANDERAS,%00000001,OK_MEDICION ;si CAMBIO_MODO=0 no hay que actualizar pantallas ni variables
    BCLR BANDERAS %00000001 ; CAMBIO_MODO<--0 para que se actualice una unica vez
    BSET TSCR2 %10000000 ;TOI <--1 habilita contador de tiempo
    BSET PIEH %00001000 ;se habilita interrupcion de PH3 (S1)
    BCLR PIEH %00000001 ;se deshabilita interrupcion de PH0 (S2) porque se activa hasta que se pase por S1.
    CLR VELOC
    BCLR BANDERAS+1 %00110000 ;ALERTA <-- 0 CALC_TICKS <-- 0
    BCLR BANDERAS %00000010 ; OUT_RANGE <-- 0
    MOVB #$BB,BIN1 ;apaga DISP3 y DISP4
    MOVB #$BB,BIN2 ;apaga DISP1 y DISP2
    MOVB #$02,LEDS ;PB1=ON en modo medicion y solo ese LED debe estar encendido
    LDX #MEDI_MSJ1
    LDY #ESPERA_MSJ
    JSR Cargar_LCD
    BRA FIN_MODO_MEDI

OK_MEDICION:
    TST VELOC
    BEQ FIN_MODO_MEDI
    JSR PANT_CTRL
FIN_MODO_MEDI:
    RTS
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina LEER_MODO: esta subrutina calcula el estado de las banderas
;     MODO1, MODO0 y CAMBIO_MODO en cada iteracion del programa principal. Esto
;     se logra leyendo los valores de PH7 y PH6.
;     Paso de parametros por direccionamiento directo a memoria: CAMBIO_MODO (O)
;     MODO1 (I/O), MODO0 (I/O)
;------------------------------------------------------------------------------
LEER_MODO:
    TST V_LIM
    BEQ FIN_LEER_MODO
    LDAA #$C0
    ANDA PTIH ;se extraen PH7 y PH6
    LDAB #$C0
    ANDB BANDERAS+1 ;se extraen MODO1 y MODO0
    CBA
    BEQ FIN_LEER_MODO ;si son iguales no hubo cambio de modo
    BSET BANDERAS %00000001 ;CAMBIO_MODO <-- 1
    BCLR BANDERAS+1 %11000000 ;MODO1 <-- 0 y MODO0 <-- 0
    ORAA BANDERAS+1 ;MODO1 <-- PH7 y MODO <-- PH6
    STAA BANDERAS+1
    LDAA CLEAR_LCD ;Cargar comando de limpiar pantalla
    JSR Send_Command ;enviar comando de limpiar pantalla
    MOVB D2ms,Cont_Delay ;luego de enviar comando limpiar pantalla se debe esperar 2ms
    JSR Delay

FIN_LEER_MODO:
    RTS
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina PANT_CTRL: se compone de cuatro partes: comprobacion de rango,
;     encendido de alarma, calculo de ticks y despliegue de infromacion.
;     Paso de parametros por direccionamiento directo a memoria: VELOC (I), V_LIM (I)
;     BIN1 (I/O), BIN2 (I/O), OUT_RANGE (I/O), CALC_TICKS (I/O), PANT_FLAG (I), TICK_EN (O),
;     TICK_DIS (O).
;------------------------------------------------------------------------------
PANT_CTRL:
    ;BCLR PIEH %00001000 ;se deshabilita la interrupcion de PH3
    LDAA #30 ;limite inferior del rango de velocidad
    LDAB #99 ;limit superior del rango de velocidad
    CMPA VELOC ;se comprueba el limite inferior
    BHI FUERA_RANGO
    CMPB VELOC ;se comprueba el limite superior
    BLO FUERA_RANGO

    BCLR BANDERAS %00000010 ;OUT_RANGE <-- 0
    ;Esto es para mostrar la alerta en cuanto se pasa por S2.
    LDAA V_LIM
    CMPA VELOC
    BHS CALC_EN_DIS
    BSET BANDERAS+1 %00010000 ;ALERTA <-- 1
    ;Hasta aqui

CALC_EN_DIS:
    BRCLR BANDERAS+1 %00100000 CALC; Si CALC_TICKS=0 hay que calcular los ticks
PANT:
    LDAA #$BB
    BRSET BANDERAS+1 %00001000 MOST_MEDI ;si PANT_FLAG=1 se debe mostra la medicion
    CMPA BIN1 ;compruebo si VELOC es un numero valido. (Implica que sigue en los primeros 100m)
    LBEQ FIN_PANT_CTRL ;si VELOC ya es $BB significa que ya se supero la pantall y se debe borrar todo
    MOVB #$BB BIN1  ;apaga DISPS derecha
    MOVB #$BB BIN2  ;apaga DISPS IZQ
    CLR VELOC ;borra velocidad para esperar a siguiente vehiculo
    BCLR BANDERAS+1 %00110000 ;ALERTA <-- 0 CALC_TICKS <-- 0
    BCLR BANDERAS %00000010 ; OUT_RANGE <-- 0
    BSET PIEH %00001000 ;habilita inetrrupcion de PH3 (S1)
    LDX #MEDI_MSJ1
    LDY #ESPERA_MSJ
    JSR Cargar_LCD ;actualiza LCD
    BRA FIN_PANT_CTRL

MOST_MEDI:
    CMPA BIN1 ;si BIN1 es $BB es porque ya el conductor esta bajo la pantalla
    BNE FIN_PANT_CTRL ;si BIN1 no es $BB es porque se debe seguir desplegando la medicion

    ;Esto es para mostrar la alerta a los 100m en lugar de en S2.
    ; LDAA V_LIM
    ; CMPA VELOC
    ; BHS MOST_INFO
    ; BSET BANDERAS+1 %00010000 ;ALERTA <-- 1
    ;Hasta aqui.

MOST_INFO:
    MOVB V_LIM,BIN1 ;velocidad limite en DISPS de derecha
    MOVB VELOC,BIN2 ;velocidad medidad en DISPS de izquierda
    LDX #MEDI_MSJ1
    LDY #MEDI_MSJ2
    JSR Cargar_LCD
    BRA FIN_PANT_CTRL

FUERA_RANGO:
    BRSET BANDERAS %00000010 PANT ;si OUT_RANGE = 1 no se debe volver a hacer la asignacion
    MOVW #$00,TICK_EN
    MOVW #92,TICK_DIS
    MOVB #$AA,VELOC
    BSET BANDERAS %00000010 ;OUT_RANGE <-- 1
    BRA PANT

CALC:
    LDAA #218 ;T_TOI escalado por 10000
    LDAB VELOC
    MUL ;D <-- A•B = T_TOI•VELOC•10000 = VELOC•218
    PSHD ;se apila el denominador
    LDAA #100 ;d = 100m, distancia para TICK_EN=1
    LDAB #36 ;factor de conversion km/h a m/s escalado por 10
    MUL ;D <-- A•B = d•36 = 100•36
    LDY #1000
    EMUL ;Y:D <-- D•Y = 100•36•1000
    PULX ;se carga el denomidor
    EDIV ;Y <-- (Y:D)/X = (36•100•1000)÷(VELOC•218) = (3.6•100•10000)÷(VELOC•218) = (3.6•100)÷(VELOC•T_TOI) = TICK_EN
    TFR Y,D
    STD TICK_EN
    LSLD ;multiplico por dos estos ticks porque es para 200m
    STD TICK_DIS
    BSET BANDERAS+1 %00100000 ;CALC_TICKS <-- 1
FIN_PANT_CTRL:
    RTS
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina BCD_BIN: el arreglo Num_Array corresponde a un numero en BCD donde
;     cada entrada es un digito. Esta subrutina toma este arreglo y calcula en
;     binario el valor numerico del arreglo. El resultado se almacena en V_LIM.
;     Paso de parametros por direccionamiento directo a memoria: V_LIM (O)
;     Paso de parametros por direccionamiento indirecto a memoria: Num_Array (I)
;------------------------------------------------------------------------------
BCD_BIN:
    LDX #Num_Array ;direccion base del numero en BCD a convertir
    CLRA ;iterador
    CLRB ;acumulador para el resultado
CIFRA:
    ADDB A,X ;se suma el digito al acumulado
    INCA ;se incrementa el indice
    PSHB ;se protege el acumulado del resultado
    LDAB #$FF ;valor que marca el final del arreglo Num_Array
    CMPB A,X  ;se compara el valor final con el proximo valor de Num_Array.
    BEQ FIN_BCD_BIN ;si son iguales es porque no hay mas numeros en el arreglo y se puede terminar la conversion
    CMPA MAX_TCL ;se compara la cantidad de valores procesados con la canitdad maxima
    BEQ FIN_BCD_BIN ;si son iguales es porque no hay mas numeros en el arreglo y se puede terminar la conversion
    PULB ;se recupera el acumulado del resultado
    PSHA ;se protege el indice
    LDAA #10 ;se prepara el multiplicador.
    MUL ;se multiplica por 10 el acumulador. En BCD cada digito esta en base 10.
    PULA ;se recupera el indice
    BRA CIFRA ;se repite lo anterior
FIN_BCD_BIN:
    PULB ;se recupera el acumulado del resultado
    STAB V_LIM ;se guarda el resultado en la variable designada
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina BIN_BCD: esta subrutina realiza la conversion de un numero
;     binario entre 0 y 99 (inclusivos) a su representacion en BCD. El numero
;     a convertir se recibe como parametro por el registro A. El resultado en
;     BCD se devuelve por memoria en la variable BCD_L, donde el nibble mas
;     significativo son las decenas y el menos significativo las unidades.
;------------------------------------------------------------------------------
BIN_BCD:
    CLR BCD_L ;para el resultado.
    LDAB #7 ;contador de desplazamientos.
OTRO_BIT:
    LSLA ;se extrae un bit del numero binario y queda en C.
    ROL BCD_L ;se inserta el bit en el resultado
    PSHA ;se protege el numero binario que se esta convirtiendo
    LDAA BCD_L ;ahora A tiene el contenido del resultado
    ANDA #$0F ;en el nibble menos significativo de A se tienen los 4 bits correspondientes a las unidades del resultado.
    CMPA #$05
    BLO DECENAS ;si el campo de las unidades es menor que 5 se puede continuar a analizar las decenas
    ADDA #$03 ;cuando las unidades son 5 o mas se deben sumar 3 unidades.
DECENAS:
    STAA LOW ;se conservan temporalmente las unidades.
    LDAA BCD_L ;se vuelve a cargar en A el contenido del resultado
    ANDA #$F0 ;en el nibble mas significativo de A se tienen los 4 bits correspondientes a las decenas del resultado.
    CMPA #$50
    BLO CONFECCIONAR ;si el campo de las unidades es menor que 5 se puede continuar a analizar las decenas
    ADDA #$30 ;cuando las decenas son 5 o mas se deben sumar 3 decenas.
CONFECCIONAR:
    ADDA LOW ;se suman las unidades
    STAA BCD_L ;se almacena el resultado
    PULA ;se recupera el numero binario que se esta convirtiendo
    DBNE B,OTRO_BIT ;se decrementa el contador de desplazamientos; cuando no es cero significa que quedan bits por analizar.
    LSLA ;se extrae el ultimo bit
    ROL BCD_L ;se inserta el ultimo bit en el resultado final.
    RTS
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina CONV_BIN_BCD: recibe como parametros de entrada las variables BIN1 y
;     BIN2 y realiza la conversion a BCD de cada una de estas variables. Luego
;     de la conversion, si el numero es menor que 10 significa que el display de
;     7 segmentos utilizado para las decenas no es necesario que este encendido;
;     en este caso se escribe $B en el nibble mas significativo de BCD1 y BCD2
;     para indicarlo. Cuando BIN1 y/o BIN2 sean $AA o $BB, este sera el mismo
;     valor de salida en BCD1 y/o BCD2, para mostrar "--" o apagar los displays.
;------------------------------------------------------------------------------
CONV_BIN_BCD:
    LDAA BIN1 ;se carga parametro de entrada a BIN_BCD
    CMPA #$AA
    BEQ TRF_BCD1 ;cuando BIN1 es $AA no se hace nada
    CMPA #$BB
    BEQ TRF_BCD1 ;cuando BIN1 es $BB no se hace nada
    JSR BIN_BCD
    LDAA BCD_L
    CMPA #10
    BHS TRF_BCD1 ;si el numero es mayor o igual a 10 no hay que apagar ningun display
    ORAA #$B0 ;se pone $B en nibble mas significativo para indicar que el display se debe apagar.
TRF_BCD1:
    STAA BCD1 ;se guarda resultado en variable de salida
    LDAA BIN2 ;se carga parametro de entrada a BIN_BCD
    CMPA #$AA
    BEQ TRF_BCD2 ;cuando BIN2 es $AA no se hace nada
    CMPA #$BB
    BEQ TRF_BCD2 ;cuando BIN2 es $BB no se hace nada
    JSR BIN_BCD
    LDAA BCD_L
    CMPA #10
    BHS TRF_BCD2 ;si el numero es mayor o igual a 10 no hay que apagar ninguno display
    ORAA #$B0 ;se pone $B en nibble mas significativo para indicar que el display se debe apagar.
TRF_BCD2:
    STAA BCD2 ;se guarda resultado en variable de salida
    RTS
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina BCD_7SEG: esta subrutina se encarga de tomar los valores en BCD1
;     y BCD2 y determinar el valor de DISP1, DISP2, DISP3, DISP4. Estas ultimas
;     cuatro variables son las que indican cuales segmentos de los displays se
;     deben encender para que se muestre el numero deseado. Sencillamente se
;     se analiza cada nibble de BCD1 y BCD2, y se toman decisiones a partir de
;     sus valores. En modo CONFIG se apagan DISP1 y DISP2.
;------------------------------------------------------------------------------
BCD_7SEG:
    LDX #SEGMENT ;direccion base de los valores para escribir en el puerto B.

    LDAB #$0F ;mascara para nibble menos significativo
    ANDB BCD1 ;se extrae el nibble menos significativo de BCD1.
    MOVB B,X,DISP4

    LDAA #$F0 ;mascara para nibble mas significativo
    ANDA BCD1 ;se extrae el nibble mas significativo de BCD1.
    LSRA
    LSRA
    LSRA
    LSRA ;se traslada el nibble mas significativo a la parte baja del byte.
    MOVB A,X,DISP3

    LDAB #$0F ;mascara para nibble menos significativo
    ANDB BCD2 ;se extrae el nibble menos significativo de BCD1.
    MOVB B,X,DISP2

    LDAA #$F0 ;mascara para nibble mas significativo
    ANDA BCD2 ;se extrae el nibble mas significativo de BCD1.
    LSRA
    LSRA
    LSRA
    LSRA ;se traslada el nibble mas significativo a la parte baja del byte.
    MOVB A,X,DISP1
FIN_BCD_7SEG:
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Subrutina TAREA_TECLADO: En esta subrutina se da la lectura del teclado. Aqui
;     se lee el teclado en el puerto A, se suprimen los rebotes, y se maneja la
;     situacion de tecla retenida.
;     Paso de parametros por direccionamiento directo a memoria: Cont_Reb (I/O)
;     Paso de parametros por direccionamiento indirecto a memoria: Num_Array (O)
;------------------------------------------------------------------------------
TAREA_TECLADO:  TST Cont_Reb
                BNE FIN_TAREA ;si el contador no ha llegado a cero no se han suprimido los rebotes.
                JSR MUX_TECLADO ;se lee el teclado en el puerto A.
                LDAA Tecla
                CMPA #$FF ;el valor $FF indica que no hay tecla presionada.
                BEQ GUARDAR ;si se toma el salto es porque es posible que haya una tecla lista para ser guardada en el arreglo. Al hacerlo solo cuando Tecla = $FF se evita la situacion de tecla retenida.
                BRSET BANDERAS+1,%00000010,CONFIRMACION ;cuando la bandera TCL_LEIDA=1 se debe confirmar que la tecla antes y despues de los rebotes es la misma
                MOVB Tecla,Tecla_IN ;se almacena el valor de la tecla antes de suprimir rebotes
                BSET BANDERAS+1 %00000010 ;TCL_LEIDA<-1 para indicar que se esta esperando a que termine la supresion de rebotes.
                MOVB #20,Cont_Reb ;se inicia la cuenta de ticks para la supresion de rebotes.
FIN_TAREA:      RTS

GUARDAR:        BRCLR BANDERAS+1,%00000001,FIN_TAREA ;si TCL_LISTA=0 no hay nada que hacer
                BCLR BANDERAS+1 %00000011 ;se limpian lsa banderas TCL_LISTA y TCL_LEIDA
                JSR FORMAR_ARRAY
                BRA FIN_TAREA

CONFIRMACION:   LDAA Tecla
                CMPA Tecla_IN
                BEQ LECTURA ;si la tecla antes y despues de la supresion de rebotes es igual entonces se dio una lectura correcta
                MOVB #$FF,Tecla ;se pone el valor nulo para la tecla
                MOVB #$FF,Tecla_IN ;se pone el valor nulo para la tecla
                BCLR BANDERAS+1 %00000011 ;se limpian lsa banderas TCL_LISTA y TCL_LEIDA
                BRA FIN_TAREA
LECTURA:        BSET BANDERAS+1 %00000001 ;TCL_LISTA=1 indica una lectura correcta del teclado.
                MOVB #20,Cont_Reb ;se inicia la cuenta de ticks para la supresion de rebotes al liberar.
                BRA FIN_TAREA
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
; Subrutina MUX_TECLADO: esta subrutina es la que se encarga de leer el teclado
;     en el puerto A. Como se habilitaron las resistencias de pull up, la parte
;     baja del puerto A (la entrada) siempre tendra valor %1111. Al colocar un
;     unico cero en alguno de los bits de la parte alta, si se presiona un boton
;     entonces se tendra un cero tambien en algun bit de la parte baja. Ambos
;     ceros en parte alta y baja definen la fila y columna de la tecla presionada.
;     Con esta informacion se calcula la posicion en el arreglo de teclas en el
;     que se encuentra el valor de la tecla presionada. El valor de la tecla se
;     devuelve en la variable Tecla. Si no se presiona nada entonces Tecla = $FF.

;     Paso de parametros por direccionamiento indirecto a memoria: Teclas (I)
;     Paso de parametros por direccionamiento directo a memoria: Tecla (O)
;------------------------------------------------------------------------------
MUX_TECLADO:    MOVB #$EF,PATRON  ;se inicializa el indice/patron
                LDAA #$F0 ;condicion de parada del loop. Luego de desplazar a PATRON cuatro veces este es el valor.
                CLRB   ;se inicaliza el offset por acumulador B
LOOP_PATRONES:  MOVB PATRON,PORTA
                BRCLR PORTA,%00000100,COLUMN2 ;si PA2=0 es porque se presiono una tecla en la tercer columna.
                BRCLR PORTA,%00000010,COLUMN1 ;si PA1=0 es porque se presiono una tecla en la segunda columna.
                BRCLR PORTA,%00000001,COLUMN0 ;si PA0=0 es porque se presiono una tecla en la primer columna.
                LSL PATRON;se desplaza hacia la izquierda el patron para avanzar de fila en el teclado
                ADDB #3 ;al avanzar de fila es neceasrio avanzar el offset los 3 espacios correspondientes a las 3 columnas
                CMPA PATRON ;se compara PATRON con $F0, la condicion de parada
                BNE LOOP_PATRONES ;si PATRON=$F0 es porque se recorrio todo el teclado y no se presiono ninguna tecla.
                MOVB #$FF,Tecla ;se carga el valor nulo para indicar que no se presiono una tecla
FIN_MUX:        RTS

COLUMN2:        INCB ;cuando la tecla esta en la tercer columna hay que incrementar el offset en 2.
COLUMN1:        INCB ;cuando la tecla esta en la segunda columna hay que incrementar el offset en 1.
COLUMN0:        LDX #Teclas
                MOVB B,X,Tecla ;se guarda en Tecla el valor encontrado por offset de acumulador.
                BRA FIN_MUX
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
; Subrutina FORMAR_ARRAY: recibe en la variable Tecla_IN el valor de la tecla
;     presionada que se quiere almacenar en array. Este arreglo representa un
;     numero en formato BCD. Se cuida que el arreglo no supere el tamano maximo.
;------------------------------------------------------------------------------
FORMAR_ARRAY:   LDX #Num_Array ;direccion base del arreglo a formar
                LDAA Tecla_IN ;la tecla presionada
                LDAB Cont_TCL ;la cantidad de valores ingresados hasta el momento
                CMPA #$0B
                BEQ BORRAR  ;se revisa si se desea borrar un valor
                CMPA #$0E
                BEQ CERRAR_ARRAY ;se revisa si se desea terminar de ingresar valores
                CMPB MAX_TCL  ;si no se dio niguno de los saltos anteriores es porque se trata de una tecla numerica
                BEQ FIN_FORMAR  ;se comprueba que haya espacio para valores nuevos, si no lo hay entonces termina la subrutina
                MOVB Tecla_IN,B,X ;se guarda el nuevo valor ingresado.
                INC Cont_TCL
FIN_FORMAR:     RTS

BORRAR:         CMPB #$00 ;para borrar primero es necesario que hayan datos que se puedan borrar
                BEQ FIN_FORMAR ;si no hay valores en el arreglo entonces termina la subrutina
                DECB
                MOVB #$FF,B,X ;se remplaza el ultimo valor ingresado por le valor nulo $FF.
                DEC Cont_TCL  ;se descuenta la cantidad de valores en el arreglo.
                BRA FIN_FORMAR

CERRAR_ARRAY:   CMPB #$00 ;para cerrar el arreglo es necesario que se haya ingresado correctamente al menos un valor.
                BEQ FIN_FORMAR ;si el arreglo esta vacio entonces termina la subrutina sin indicar que se cerro el arreglo
                CMPB MAX_TCL
                BEQ OK
                MOVB #$FF,B,X ;cuando el arreglo tiene menos valores que la cantidad maxima es necesario indicar el final.
OK:             BSET BANDERAS+1 %00000100 ;ARRAY_OK=1 para indicar que se cerro el arreglo y no se deben leer mas teclas.
                CLR Cont_TCL ;para prepararlo para una futura lectura del teclado.
                BRA FIN_FORMAR
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
;   Subrutina Delay: se mantiene en un loop cerrado hasta que Cont_Delay sea 0.
;     Cont_Delay es descontado por OC4 a 50 kHz.
;     Paso de parametros por direccionamiento directo a memoria: Cont_Delay (I)
;------------------------------------------------------------------------------
Delay:
    TST Cont_Delay
    BNE DELAY
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina Send_Command: se encarga de enviar al LCD el comando que recibe
;     por el acumulador A.
;------------------------------------------------------------------------------
Send_Command:
    PSHA ;el comando se recibe en acumulador A y se protege para poder analizar sus nibbles por separado
    ANDA #$F0 ;Se deja solo el nibble superior del comando a ejecutar
    LSRA
    LSRA ;se alinea nibble con bus datos en PORTK5-PORTK2.
    STAA PORTK ;se carga parte alta del comando en el bus de datos.
    BCLR PORTK,$01 ;Se habilita el envío de comandos.
    BSET PORTK,$02 ;Se habilita comunicacion con la LCD.
    MOVB D260us,Cont_Delay ;se inicia el retardo de 260us.
    JSR Delay
    BCLR PORTK,$02 ;Se deshabilita comunicacion con la LCD
    PULA ;se recupera el comando original de la pila
    ANDA #$0F ;Se deja solo el nibble inferior del comando a ejecutar
    LSLA
    LSLA ;se alinea nibble con bus datos en PORTK5-PORTK2.
    STAA PORTK ;se carga parte baja del comando en el bus de datos.
    BSET PORTK,$02 ;Se habilita comunicacion con la LCD.
    MOVB D260us,Cont_Delay ;se inicia el retardo de 260us.
    JSR Delay
    BCLR PORTK,$02 ;Se deshabilita comunicacion con la LCD
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina Send_Data: se encarga de enviar al LCD el dato que recibe
;     por el acumulador A.
;------------------------------------------------------------------------------
Send_Data:
    PSHA ;el dato se recibe en acumulador A y se protege para poder analizar sus nibbles por separado
    ANDA #$F0 ;Se deja solo el nibble superior del dato
    LSRA
    LSRA ;se alinea nibble con bus datos en PORTK5-PORTK2.
    STAA PORTK ;se carga parte alta del dato en el bus de datos.
    BSET PORTK,$01 ;Se habilita el envío de dato.
    BSET PORTK,$02 ;Se habilita comunicacion con la LCD.
    MOVB D260us,Cont_Delay ;se inicia el retardo de 260us.
    JSR Delay
    BCLR PORTK,$02 ;Se deshabilita comunicacion con la LCD
    PULA ;se recupera el dato original de la pila
    ANDA #$0F ;Se deja solo el nibble inferior del dato
    LSLA
    LSLA ;se alinea nibble con bus datos en PORTK5-PORTK2.
    STAA PORTK ;se carga parte baja del dato en el bus de datos.
    BSET PORTK,$01 ;Se habilita envío de datos
    BSET PORTK,$02 ;Se habilita comunicacion con la LCD.
    MOVB D260us,Cont_Delay ;se inicia el retardo de 260us.
    JSR Delay
    BCLR PORTK,$02 ;Se deshabilita comunicacion con la LCD
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina INIT_LCD: se encarga de inicializar la pantalla LCD ejecutando la
;     secuencia correcta de comandos.
;     Paso de parametros por direccionamiento indirecto a memoria: iniDsp (I)
;------------------------------------------------------------------------------
INIT_LCD:
    LDX #iniDsp+1 ;Se carga en X la tabla que contiene los comandos de inicialización. Posicion 0 tiene el tamano de la tabla.
    CLRB
COMMANDS:
    LDAA B,X ;Se recorren los comandos con direccionamiento indexado por acumulador B
    JSR Send_Command ;Se ejecuta cada comando
    MOVB D40us,Cont_Delay ;40us son necesarios luego de enviar cualquiera de los comando de inicializacion
    JSR Delay
    INCB ;siguiente comando
    CMPB iniDsp
    BNE COMMANDS ;Si ya se ejecutaron todos los comandos de la tabla, terminar comandos de inicialización
    LDAA CLEAR_LCD ;Cargar comando de limpiar pantalla
    JSR Send_Command ;enviar comando de limpiar pantalla
    MOVB D2ms,Cont_Delay ;luego de enviar comando limpiar pantalla se debe esperar 2ms
    JSR Delay
    RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina Cargar_LCD: esta subrutina se encarga de enviar a la pantalla LCD
;     cada caracter, uno por uno, de ambas lineas del LCD. Recibe los parametros
;     en los registros indice X y Y, que contienen las direcciones de inicio a
;     los mensajes de las lineas 1 y 2 respectivamente.
;------------------------------------------------------------------------------
Cargar_LCD:
    LDAA ADD_L1	;Se carga la dirección de la primera posición de la primera fila de la LCD
		JSR Send_Command ;Se ejecuta el comando
		MOVB D40uS,Cont_Delay
		JSR Delay
LINE1:
    LDAA 1,X+ ;Se carga cada caracter en A
		BEQ CARG_2 ;Si se encuentra un caracter de EOM ($00) se terminó de imprimir la primera fila
		JSR Send_Data ;Se imprime cada caracter
		MOVB D40us,Cont_Delay
		JSR Delay
		BRA LINE1
CARG_2:
    LDAA ADD_L2 ;Se carga la dirección de la primera posición de la segunda fila de la LCD
		JSR Send_Command
		MOVB D40us,Cont_Delay
		JSR Delay
LINE2:
    LDAA 1,Y+ ;Se carga cada caracter en A
		BEQ FIN_Cargar_LCD ;Si se encuentra un caracter de EOM ($00) se terminó de imprimir la primera fila
		JSR Send_Data ;Se imprime cada caracter
		MOVB D40us,Cont_Delay
		JSR Delay
		BRA LINE2
FIN_Cargar_LCD:
	  RTS
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;   Subrutina PATRON_LEDS: esta subrutina se encarga de actualizar el valor de
;     la variable LEDS para desplegar correctamente el modo de operacion y el
;     barrido de LEDS cuando se debe mostrar la alerta
;     Paso de parametros por direccionamiento directo a memoria: LEDS (I/O), ALERTA (I)
;------------------------------------------------------------------------------
PATRON_LEDS:
    LDAA #$07 ;mascara
    ANDA LEDS ;se extraen LEDS PB0 a PB2 (Modo de operacion)
    BRCLR BANDERAS+1 %00010000 FIN_PATRON_LEDS ;si ALERTA = 0 no hay nada que hacer
    LDAB #$F8 ;mascara
    ANDB LEDS ;se extraen LEDS PB7 a PB3 (Indicadores de alerta). Afecta la bandera Z.
    BEQ PRIMER_BIT ;cuando los bits indicadores son cero hay que cargar el bit por primea vez.
    LSRB ;rotamos el bit indicador.
    CMPB #$04
    BNE UNIR_LEDS
PRIMER_BIT:
    LDAB #$80 ;reinicia el bit indicador.
UNIR_LEDS:
    ABA ;reincorpora los LEDS indicadores de alerta y de modo de operacion
FIN_PATRON_LEDS:
    STAA LEDS
    RTS
;------------------------------------------------------------------------------
